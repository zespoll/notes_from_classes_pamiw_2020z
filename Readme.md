# Zajęcia PAMiW 2020Z (1DI1541)

1. [Lab_1](#markdown-header-lab-1-heroku)
2. [Lab_2](#markdown-header-lab-2-html)
3. [Lab_3](#markdown-header-lab-3-ajax)
4. [Lab_4](#markdown-header-lab-4-server-side)
5. [Lab_5](#markdown-header-lab-5-redis)
6. [Lab_6](#markdown-header-lab-6-redis-and-session)
7. [Lab_7](#markdown-header-lab-7-jwt)
8. [Lab_8](#markdown-header-lab-8-rest)
9. [Lab_9](#markdown-header-lab-9-responsywne)
10. [Lab_10](#markdown-header-lab-10-progresywne)
11. [Lab_11](#markdown-header-lab-11-gniazda)
12. [Lab_12](#markdown-header-lab-12-autoryzacja)
13. [Lab_13](#markdown-header-lab-13-kolejki)

## Lab 1 Heroku

Pierwsze zajęcia są poświęcone zorganizowaniu sobie środowiska pracy (przy zajęciach zdalnych).
Spróbujemy zrealizować prosty przykład z użyciem dwóch niezależnych technologii. Najpierw napiszemy i uruchomimy aplikację lokalnie (tu skorzystamy z Docker-a), a później spróbujemy naszą aplikację wdrożyć na zdalny serwer (tu wykorzystamy Heroku).
Na kolejnym spotkaniu zastanowimy się, czy (i jak) można to zrobić sprawniej.

### Pierwszy obraz docker-owy
Nim zaczniemy, powinniśmy mieć pewność, że [Docker](https://docs.docker.com/engine/install/) jest już zainstalowany. Dzisiaj potrzebujemy tylko podstawowy zestaw (`Docker Engine` oraz `Docker CLI`), choć w przyszłości będziemy jeszcze chcieli skorzystać z `Docker Compose`.

Czym jest Docker? Na potrzeby naszego pierwszego uruchomienia powiedzmy sobie, że Docker daje nam możliwość tworzenia obrazów różnych konfiguracji środowisk uruchomieniowych (czegoś w rodzaju lekkich wirtualnych maszyn). Takie gotowe obrazy możemy później uruchamiać (kolejne instancje tych obrazów). Te instancje będziemy nazywać kontenerami.

Tyle nam wystarczy na początek. Teraz spróbujmy stworzyć naszą pierwszą aplikację w Python-ie. Aby było nam łatwiej, skorzystamy z pomocy, którą nam oferuje `Flask` (szkielet / platforma / framework do tworzenia aplikacji webowych w Python-ie).

#### Wykonajmy kolejne kroki (od zera, do uruchomionego kontenera)
1. Stwórzmy główny katalog dla naszego projektu (np. `Lab_1_example`).
2. Wewnątrz katalogu stwórzmy trzy pliki: `requirements.txt`, `app.py`, `Dockerfile`. Pierwszy z tych plików będzie zawierał listę zależności (pakietów Python-a), z których będziemy chcieli skorzystać. Drugi plik (`app.py`) będzie sercem naszej aplikacji, więc umieścimy w nim kod źródłowy. Trzeci plik (`Dockerfile`) jest plikiem konfiguracyjnym dla stworzenia obrazu Docker-owego.
Struktura naszego katalogu powinna wyglądać następująco:

```
.
├── app.py
├── Dockerfile
└── requirements.txt
```
    
3. Uzupełnijmy pliki

##### requirements.txt

```sh
Flask
```
    
##### Dockerfile

```sh
FROM python:2.7-slim
WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt
CMD ["python", "app.py"]
```

##### app.py

```python
# -*- coding: utf-8 -*-
from flask import Flask, request

app = Flask(__name__)

available_languages = ["pl", "en", "es", "de"]


@app.route("/")
def index():
    language = request.accept_languages.best_match(available_languages)

if language == "pl":
    message = "Witaj, Świecie!"
elif language == "en":
    message = "Hello, World!"
elif language == "es":
    message = "Hola, Mundo!"
elif language == "de":
    message = "Halo Welt!"
else:
    message = "31337"

    return "<strong>{}</strong>".format(message)


@app.route("/secret")
def secret():
    return "You're not authorized! Go away, Bro!", 401


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
```

4. Ten kod jeszcze nie musi być dla nas zrozumiały i oczywisty. W najbliższej przyszłości poświęcimy dodatkowy czas na ponowną analizę tego przykładu.

5. Spróbujmy zbudować obraz Docker-owy (oznaczony tagiem `first_app_flask`):

    ```
    user@user:$ docker build -t first_app_flask .
    ```

6. Zweryfikujmy, czy proces przebiegł poprawnie (wyświetlmy dostępne obrazy):

    ```
    user@user:$ docker images
    ```

7. Jeżeli wszystko przebiegło poprawnie, to możemy uruchomić nasz obraz (dzięki użyciu flagi `--rm` kontener zostanie usunięty po zakończeniu pracy):

    ```
    user@user:$ docker run --rm -p 5000:5000 first_app_flask:latest
    ```

8. Weryfikujemy poprawność działania w oknie przeglądarki pod adresem: `localhost:5000`.

9. W tym momencie możemy wykonać różne testy (wysyłając żądania) przy użyciu narzędzi: httpie, curl, Postman. Spróbujmy uzyskać z serwera odpowiedź z powitalną informacją w języku hiszpańskim (es).

    ```
    user@user:$ curl -X GET -H "Accept-Language: es" localhost:5000
    ```


### Spróbujmy udostępnić naszą aplikację na zewnątrz
Jest wiele możliwości, aby pozwolić innym osobom zobaczyć naszą aplikację. W ramach naszych zajęć będziemy korzystać z serwisu [Heroku](https://www.heroku.com/).
Bez wnikania w szczegóły i detale, spróbujmy wykonać kilka podstawowych operacji. Nim zaczniemy, upewnijmy się, że posiadmy konto w serwisie Heroku i mamy zainstalowany [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli).

Jeżeli już spełniamy warunki wejściowe, to wykonajmy kilka podstawowych kroków pozwalających nam na wdrożenie naszej aplikacji na zewnętrzny serwer.

1. W katalogu głównym naszego projektu (`Lab_1_example`) musimy zainicjalizować repozytorium Git-a (albo innego systemu kontroli wersji, jeśli ma się wolną godzinę)

    ```
    user@user:$ git init
    ```

2. Do naszego projektu (w katalogu głównym) dodajemy plik `Procfile` z zawartością w postaci jednego wiersza: `web: gunicorn app:app`.

3. Modyfikujemy zawartość pliku `requirements.txt` o kolejny (drugi) wiersz w postaci: `gunicorn`.

4. Dodajemy wszystkie zmiany w naszym lokalnym repozytorium i zapisujemy jako `commit`.

    ```
    user@user:$ git add .
    ```

    ```
    user@user:$ git commit -m "First version of sample flask app."
    ```

5. Jeżeli nie zrobiliśmy tego wcześniej (jest to nasze pierwsze uruchomienie Heroku po włączeniu komputera), to będziemy musieli zalogować się do serwisu, dlatego z wiersza poleceń trzeba będzie wpisać polecenie, które otworzy nam okno przeglądarki (z formularzem logowania)

    ```
    user@user:$ heroku login
    ```

6. Wykonujemy polecenie, które ma nam przygotować miejsce pod naszą aplikację na serwerze Heroku. Nazwa dla naszej aplikacji zostanie wygenerowana automatycznie, dzięki czemu mamy gwarancję, że nie dojdzie do kolizji nazw (może się zdarzyć, że nazwa, którą wybierzemy samodzielnie, będzie zajęta / niedostępna).

    ```
    user@user:$ heroku create
    ```

7. W tym momencie nasze lokalne repozytorium powinno zostać połączone z repozytorium zdalnym (możemy to zweryfikować poleceniem: `git remote -v`).

8. Jeżeli do tej pory wszystko nam się udało, to wysyłamy zmiany na serwer, a tym samym wdrażamy naszą aplikację (w odpowiedzi powinniśmy zobaczyć adres, pod którym będzie dostępna nasza aplikacja.)

     ```
    user@user:$ git push heroku master
    ```

9. Sprawdźmy, czy pod wskazanym adresem uzyskujemy komunikat "Hello, World!" (w wybranym języku).

### Wykorzystajmy `telnet`, `httpie` oraz `curl` do zweryfikowania stanu naszej aplikacji (trzeba podmienić nazwę `nazwa-mojej-aplikacji`)

```sh
user@user:$ telnet nazwa-mojej-aplikacji.herokuapp.com 443
Trying ...
Connected to nazwa-mojej-aplikacji.herokuapp.com.
Escape character is '^]'.
^]
telnet> quit
Connection closed.

user@user:$ http 'https://nazwa-mojej-aplikacji.herokuapp.com' Accept-Language:en,de,pl
user@user:$ http -v GET 'https://nazwa-mojej-aplikacji.herokuapp.com/not_found'
user@user:$ curl -v -X GET 'https://nazwa-mojej-aplikacji.herokuapp.com/secret'
user@user:$ curl -i -X GET 'https://nazwa-mojej-aplikacji.herokuapp.com/secret'
```


## Lab 2 HTML
Celem drugiego spotkania jest zapoznanie się lub przypomnienie sobie podstawowych elementów HTML-a i CSS. Dodatkowo powinniśmy spróbować napisać kilka prostych skryptów w JavaScript.

Pod koniec tego ćwiczenia:

- powinna powstać statyczna strona zbudowana ze znaczników semantycznych;
- powinniśmy napisać plik CSS definiujący układ treści w oknie klienta (przeglądarki);
- powinniśmy stworzyć plik JS z kodem ingerującym w treść strony.

Całość powinna zostać wdrożona na serwery Heroku.

### Struktura plików 

1. Należy stworzyć poniższą strukturę katalogów.

```
lab_2
├── Dockerfile
└── static_app_lab2
    ├── app.py
    ├── Procfile
    ├── requirements.txt
    ├── static
    │   ├── images
    │   ├── scripts
    │   │   └── script.js
    │   └── styles
    │       └── style.css
    └── templates
        └── index.html
```

### index.html
2. Struktura strony powinna wyglądać następująco:


```html
<!DOCTYPE html>
<html lang="pl">
    <head></head>
    <body>
        <header>
            <h2></h2>
        </header>
        <nav></nav>
        <section>
            <h3></h3>
            <p></p>
        </section>
        <article>
            <h3></h3>
            <p></p>
        </article>
        <article>
            <h3></h3>
            <p></p>
        </article>
        <article>
            <h3></h3>
            <p></p>
        </article>

        <footer>
        </footer>
    </body>
</html>
```

Znaczniki `<head></head>` uzupełniamy informacją o tytule strony oraz o kodowaniu, które umożliwi nam poprawne wyświetlanie polskich znaków diakrytycznych.

```html
<head>
    <title>Nobliści</title>
    <meta charset="utf-8">
</head>
```

1. Znaczniki `<h2></h2>` uzupełniamy treścią pozwalającą na sprawdzenie działania kodowania znaków (_zażółć gęślą jaźń_).
2. Znaczniki `<nav></nav>` wypełniamy w przykładowy sposób (dzisiaj stworzymy miejsce na trzy odnośniki -- aktualnie żaden z nich nie będzie działał)
 
```html
<nav>
    <ul>
        <li><a href="#">Info</a></li>
        <li><a href="#">Znajdź noblistę</a></li>
        <li><a href="#">Kontakt</a></li>
    </ul>
</nav>
```

Zawartość paragrafów (znaczniki `<p></p>`) wypełniamy wygenerowaną treścią [Lorem Ipsum](https://www.lipsum.com/). Pozostałe znaczniki `<h3></h3>` uzupełniamy własnym  tekstem.

Dwa ostatnie artykuły (znaczniki `<article></article>`) powinny zawierać po jednej tabeli. Pierwsza tabela ma mieć wymiary 3x3, a wewnątrz tej tabeli mają być umieszczone zdjęcia i podpisy trzech laureatów Nagrody Nobla.

```html
<table>
    <caption>Laureaci literackiej Nagrody Nobla</caption>
    <tbody>
        <tr>
            <td><img src="images/Wislawa_Szymborska.jpg" alt="Wisława Szymborska"/></td>
            <td colspan="2">Wisława Szymborska</td>
        </tr>
        <tr>
            <td colspan="2">Olga Tokarczuk</td>
            <td><img src="images/Olga_Tokarczuk.jpg" alt="Olga Tokarczuk"/></td>
        </tr>
        <tr>
            <td>Czesław Miłosz</td>
            <td><img src="images/Czeslaw_Milosz.jpg" alt="Czesław Miłosz"></td>
            <td></td>
        </tr>
    </tbody>
</table>
```

Druga tabela powinna mieć wymiary 6x4, a jej kolumny powinny określać kolejno: liczbę porządkową, Imię, Nazwisko, dyscyplinę / kategorię -- chodzi oczywiście o laureatów Nagrody Nobla, dlatego w kolejnych pięciu wierszach tabeli Trzeba wstawić wybrane przez siebie osoby. 

```html
<table class="color-table">
    <caption>Nobliści XXI wieku</caption>
    <thead>
        <tr>
            <td>Lp.</td>
            <td>Imię</td>
            <td>Nazwisko</td>
            <td>Dziedzina / kategoria</td>
        </tr>
    </thead>
    <tbody id="laureates_tbody">
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>
```

Zawartość znaczników `<footer></footer>` należy uzupełnić dwoma paragrafami (proszę odpowiednio podmienić zawartość).

```html
<footer>
    <p class="footer-inline left">&copy; 2020 Hans Castorp</p>
    <p class="footer-inline right">proudly powered by <img src="images/vim-icon.png" alt="vim icon"/></p>
</footer>
```

### CSS (`style.css`)

Poniższy kod powinien zostać umieszczony wewnątrz pliku `style.css`.

```css
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    font-family: 'Cambay', sans-serif;
    padding-bottom: 46px; /* it must be the same size as footer height */
}

nav ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    background-color: aliceblue;
}

nav ul li {
    display: inline;
}

nav ul li a {
    display: inline-block;
    width: 150px;
    text-align: center;
    text-decoration: none;
    color: brown;
}

nav ul li a:hover {
    background-color: #F0F0F0;
}

section p,
article p {
    text-indent: 25px;
}

section h3 {
    color: darkorange;
}

section p {
    color: orange;
}

article h3 {
    color: darkseagreen;
}

article p {
    color: seagreen;
}

table {
    margin-left: auto;
    margin-right: auto;
}

table, th, td {
    border: 1px solid darkgoldenrod;
}

td, img {
    vertical-align: bottom;
}

.caption-bottom {
    caption-side: bottom;
}

.color-table tr:nth-child(odd) {
    background-color: lightyellow;
}

.color-table tr:nth-child(even) {
    background-color: lightpink;
}

.color-table thead tr {
    background-color: lightgrey !important;
}

footer {
    position: fixed;
    bottom: 0;
    background-color: aliceblue;
    max-width: inherit;
    width: 100%;
    height: 46px;
    font-size: 15px;
}

.footer-inline {
    display: inline-block;
    width: 48%;
    padding-left: 1%;
}

.left {
    text-align: left;
}

.right {
    text-align: right;
}

.footer-img {
    vertical-align: middle;
}
```

Aby widoczne było działanie powyższego pliku, należy uzupełnić znaczniki `<head></head>` o dodatkowy znacznik `<link>` z atrybutami.

```html
<link href="styles/style.css" rel="stylesheet" type="text/css">
```

Dodatkowo można skorzystać z zewnętrznej (ładniejszej?) propozycji czcionki (fontu) dla poprawienia wyglądu naszej strony.
Proponuję dopisać kolejny znacznik `<link>`.

```html
<link href='http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
```

### JS (`script.js`)

```js
document.addEventListener("DOMContentLoaded", function (event) {
    let names = ["Jacek", "Robert", "Bartek", "Zuza", "Wiktor", "Paweł"];

    names.forEach(function (name, id) {
        console.log(name + " " + id);
    });
});

document.addEventListener("DOMContentLoaded", function (event) {
    let laureates = prepareLaureates();

    laureates.forEach(function (laureate, id) {
        putLaureatesIntoTable(laureate);
    });
});

function putLaureatesIntoTable(laureate) {
    let tbody = document.getElementById("laureates_tbody");
    let size = tbody.children.length;
    
    tbody.innerHTML +=
            "<tr><td>" + (size + 1)
            + "</td><td>" + laureate.name
            + "</td><td>" + laureate.surname
            + "</td><td>" + laureate.discipline
            + "</td></tr>";
}

function prepareLaureates() {
    let laureate_1 = new NobelLaureate("Andriej", "Gejm", "Fizyka");
    let laureate_2 = new NobelLaureate("Konstantin", "Nowosiołow", "Fizyka");
    let laureate_3 = new NobelLaureate("Mo", "Yan", "Literatura");
    let laureate_4 = new NobelLaureate("Barack", "Obama", "Pokojowa Nagroda Nobla");
    let laureate_5 = new NobelLaureate("Bob", "Dylan", "Literatura");

    let laureates = [laureate_1, laureate_2, laureate_3, laureate_4, laureate_5];

    return laureates;
}

function NobelLaureate(name, surname, discipline) {
    this.name = name;
    this.surname = surname;
    this.discipline = discipline;
}
```

Powyższa zawartość powinna być umieszczona wewnątrz pliku `script.js`. Aby nasz skrypt został uruchomiony, dopiszemy w znaczniku `<head></head>` znacznik `<script></script>`.

```html
<script src="scripts/script.js"></script>
```

### Walidatory (HTML, CSS)

Poprawność wykonanego kodu należy sprawdzić walidatorami W3C.

- https://validator.w3.org/
- https://jigsaw.w3.org/css-validator/
 

### Procfile

```
web: gunicorn app:app
```

### Requirements

```
Flask
gunicorn
```

### App (`app.py`)

```python
from flask import Flask, render_template

app = Flask(__name__, static_url_path="")

@app.route("/", methods=["GET"])
def index():
    return render_template("index.html")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
```

### Dockerfile

```
FROM python:3.7-slim
WORKDIR /static_app
COPY ./static_app_lab2 /static_app
RUN pip install -r requirements.txt
CMD ["python", "app.py"]
```

#### Lokalne uruchomienie projektu (wykorzystując `Docker-a`)
Chcąc loklanie uruchomić projekt przy użyciu `Docker-a`, możemy wykonać dwie poniższe instrukcje.

```sh
user@user:$ docker build . -t=lab-2-static-app
user@user:$ docker run --rm -p5000:5000 lab-2-static-app:latest
```


### Wgranie zmian na Heroku
Będąc w katalogu `static_app_lab2` wykonujemy:

```sh
user@user:$ git init
user@user:$ git add app.py Procfile requirements.txt static/ templates/
user@user:$ git commit -m "First commit with static app lab2."
user@user:$ heroku create
user@user:$ git push heroku master
```

Proszę zmienić nazwę swojej aplikacji na taką, która składa się z:

- napisu `pamiw2020z`;
- imienia;
- nazwiska.

Wszystko powinno być zapisane małymi literami bez użycia polskich znaków diakrytycznych.

Należy wykorzystać polecenie `heroku apps:rename nowa-nazwa`.
To może wyglądać tak:

```sh
user@user:$ heroku apps:rename pamiw2020zhanscastorp
```

## Lab 3 Ajax

Kolejne nasze spotkanie będzie (albo było, ponieważ już jest za nami) poświęcone asynchronicznemu wywołaniu skryptów w JavaScript. Jest to jeden z kluczowych elementów tej technologii, który sprawia, że JavaScript jest tak powszechnie obecny w aplikacjach internetowych. Wywołania asynchroniczne ulepszają działanie aplikacji internetowych, ponieważ dzięki nim korzystanie z nich (z punktu widzenia użytkownika) jest dużo przyjemniejsze. Jednocześnie takie wywołania są bardziej wymagające dla twórców tych aplikacji, ponieważ wymagają uwzględniania scenariusza: "a co się stanie, jeśli żądanie do nas nie wróci z odpowiedzią?" podczas pisania kodu.

My dzisiaj powinniśmy (jak za każdym razem) spróbować zaznajomić się z tą technologią, dlatego naszym celem będzie zbudowanie formularza rejestracji użytkowników w systemie o dziwnej nazwie: `POST-awa`. Nasz formularz powinien zawierać 4 pola przeznaczone na: `login`, `PESEL`, `hasło`, `powtórzenie hasła`.
Po naciśnięciu przycisku `zarejestruj się`, nasz formularz powinien zostać wysłany -- jeżeli przesłane dane będą prawidłowe, to rejestracja powinna zakończyć się pomyślnie.

A gdzie w tym wszystkim asynchroniczność?

Powinniśmy założyć dodatkowo dwa warunki. **Pierwszy:** sprawdzanie zajętości loginu należy robić przed wysłaniem formularza (od razu po wpisaniu loginu w formularzu powinna wyświetlić się informacja o tym, czy dany login jest dostępny, czyli zweryfikowanie, czy już ktoś wcześniej wykorzystał ten login).
**Drugi:** po naciśnięciu `zarejestruj się` strona nie zostanie przeładowana, ale żądanie rejestracji zostanie wysłane. W konsoli przeglądarki powinna pojawić się informacja o statusie wykonanej operacji. W wersji rozszerzonej tego punktu można oczekiwać, że komunikat (status rejestracji) zostanie wyświetlony na stronie (w sposób widoczny dla użytkownika).

Kod, który zrealizuje przykładową implementację, zostanie umieszczony poniżej.

Sprawdzanie dostępności loginu należy wykonać z użyciem end-point-a: `https://pamiw2020registration.herokuapp.com/user`.
Przykładowo można zweryfikować, czy istnieje użytkownik o loginie `Pawel`.

```
user@user:$ curl -X GET https://pamiw2020registration.herokuapp.com/user/Pawel
```

Do rejestracji użytkownika (do wysłania formularza) należy skorzystać z end-point-a: `https://pamiw2020registration.herokuapp.com/register`. A przykładowa rejestracja może przebiegać przy wykorzystaniu przygotowanego poniżej żądania.

```
user@user:$ curl -X POST -F 'login=Pawel' -F 'password=za2qWSX!@#' -F 'second_password=za2qWSX!@#' -F 'pesel=53101237121' https://pamiw2020registration.herokuapp.com/register
```

Do generowania różnych numerów można skorzystać z serwisu [Generatory IT](http://generatory.it/).

### Struktura katalogów
Realizacja zamieszczonego przykładu wymaga przygotowania struktury katalogów:

```
lab_3
├── Dockerfile
└── registration_static_app_lab3
    ├── Procfile
    ├── registration_app.py
    ├── requirements.txt
    ├── static
    │   ├── images
    │   │   └── vim-icon.png
    │   ├── scripts
    │   │   └── script.js
    │   └── styles
    │       └── style.css
    └── templates
        └── registration.html
```


### Dockerfile
Plik `Dockerfile` nie jest niezbędny w tym projekcie, ale jeżeli chcielibyśmy uruchomić projekt lokalnie (bez korzystania z lokalnej instancji Heroku), to możemy to wykonać z pomocą `Docker-a`.

```
FROM python:3.7-slim
WORKDIR /registration_app
COPY ./registration_static_app_lab3 /registration_app
RUN pip install -r requirements.txt
CMD ["python", "registration_app.py"]
```

Najpierw należy zbudować obraz dla `Docker-a` (wykonać polecenie stojąc w katalogu z plikiem konfiguracyjnym):

```
docker build . -t=registration_form_lab3
```

Po zbudowaniu obrazu, można zbudować i uruchomić kontener z tym obrazem:

```
docker run --rm -p5000:5000 registration_form_lab3
```

Teraz (gdy struktura plików i katalogów projektu już jest gotowa) można sprawdzić, czy pod adresem `localhost:5000` jest widoczna nasza strona z formularzem rejestracji.

### Procfile
Plik konfiguracyjny dla `Heroku` powinien zawierać tylko jedną linię -- to nam wystarczy. Po słowie kluczowym `gunicorn` występuje nazwa modułu `Python-a` (jest to nazwa naszego pliku z kodem źródłowym), a po dwukropku występuje `app`, czyli jest to nazwa zmiennej, do której odwołujemy się we wskazanym module.

```
web: gunicorn registration_app:app
```

### Registration (`registration_app.py`)

W tym pliku jest małe (ale jednak) serce naszej aplikacji, które definiuje, co ma zostać zwrócone, gdy poprosimy klienta (przeglądarkę) o wyświetlenie strony. Ten plik jest bardzo zbliżony do poprzedniej konfiguracji, dlatego jego interpretacja nie powinna sprawiać trudności.

```
from flask import Flask, render_template

app = Flask(__name__, static_url_path="")

@app.route("/", methods=["GET"])
def register():
    return render_template("registration.html")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
```

### CSS (`style.css`)

Plik definiujący nam wygląd naszej strony jest odchudzoną wersją pliku z poprzednich zajęć. Jednak należy zwrócić uwagę na kilka dodatkowych elementów. Proszę spojrzeć na dwie klasy: `.required`, `.warning-field`. Pierwsza z tych klas odpowiada za wyświetlenie "czerwonej gwiazdki" obok pola formularza, które będzie wymagane. Druga klasa odpowiada za styl elementu, w którym będzie wyświetlał się komunikat o zajętości loginu.

```
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    font-family: 'Cambay', sans-serif;
    padding-bottom: 46px; /* it must be the same size as footer height */
}

nav ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    background-color: aliceblue;
}

nav ul li {
    display: inline;
}

nav ul li a {
    display: inline-block;
    width: 150px;
    text-align: center;
    text-decoration: none;
    color: brown;
}

nav ul li a:hover {
    background-color: #F0F0F0;
}

section h3 {
    color: darkorange;
}

.required::after {
    content: "*";
    color: red
}

.warning-field {
    display: inline-block;
    width: 200px;
    color: red;
    font-size: small;
}

.form-row {
    margin-top: 10px;
}

footer {
    position: fixed;
    bottom: 0;
    background-color: aliceblue;
    max-width: inherit;
    width: 100%;
    height: 46px;
    font-size: 15px;
}

.footer-inline {
    display: inline-block;
    width: 48%;
    padding-left: 1%;
}

.left {
    text-align: left;
}

.right {
    text-align: right;
}

.footer-img {
    vertical-align: middle;
}
```

### JS (`script.js`)

Plik `script.js` składa się z kilku funkcji. To właśnie przy pomocy kodu z tego pliku odbywa się wysyłanie asynchronicznych żądań do serwera.

W pliku zdefiniowane są stałe oraz globalne zmienne:

```js
    const GET = "GET";
    const POST = "POST";
    const URL = "https://pamiw2020registration.herokuapp.com/";

    const LOGIN_FIELD_ID = "login";

    var HTTP_STATUS = {OK: 200, CREATED: 201, NOT_FOUND: 404};

    let registrationForm = document.getElementById("registration-form");
```

Po naciśnięciu przycisku wysyłającego formularz, w konsoli przeglądarki wyświetlą się wszystkie wartości, którymi wypełniono formularz. I, co oczywiste, zostanie wysłany formularz.

```js
    registrationForm.addEventListener("submit", function (event) {
        event.preventDefault();

        console.log("Form submission stopped.");

        var n = event.srcElement.length;
        for (var i = 0; i < n; i++) {
            console.log(event.srcElement[i].value);
        }

        submitRegisterForm();
    });

```

Wysłanie formularza jest wykonane osobną funkcją, która wykorzystuje mechanizm obietnic (`Promise`). Zrozumienie tego elementu jest kluczowe przy późniejszej pracy z wywołaniami asynchronicznymi (i dalszą częścią tego przykładu). Z tego powodu proszę dokładnie przeanalizować cały kod JavaScript z tego przykładu.

```js
    function submitRegisterForm() {
        let registerUrl = URL + "register";

        let registerParams = {
            method: POST,
            body: new FormData(registrationForm),
            redirect: "follow"
        };

        fetch(registerUrl, registerParams)
                .then(response => getRegisterResponseData(response))
                .then(response => displayInConsoleCorrectResponse(response))
                .catch(err => {
                    console.log("Caught error: " + err);
                });
    }

    function getRegisterResponseData(response) {
        let status = response.status;

        if (status === HTTP_STATUS.OK || status === HTTP_STATUS.CREATED) {
            return response.json();
        } else {
            console.error("Response status code: " + response.status);
            throw "Unexpected response status: " + response.status;
        }
    }

    function displayInConsoleCorrectResponse(correctResponse) {
        let status = correctResponse.registration_status;

        console.log("Status: " + status);

        if (status !== "OK") {
            console.log("Errors: " + correctResponse.errors);
        }
    }
```

Do obsługi zdarzenia zmiany loginu służy zestaw kolejnych funkcji, które mają dodawać lub usuwać pole z komunikatem o zajętości loginu.

```js
   function prepareEventOnLoginChange() {
        let loginInput = document.getElementById(LOGIN_FIELD_ID);
        loginInput.addEventListener("change", updateLoginAvailabilityMessage);
    }

    function updateLoginAvailabilityMessage() {
        let warningElemId = "loginWarning";
        let warningMessage = "This login is already taken.";

        isLoginAvailable().then(function (isAvailable) {
            if (isAvailable) {
                console.log("Available login!");
                removeLoginWarningMessage(warningElemId);
            } else {
                console.log("NOT available login");
                showLoginWarningMessage(warningElemId, warningMessage);
            }
        }).catch(function (error) {
            console.error("Something went wrong while checking login.");
            console.error(error);
        });
    }

    function showLoginWarningMessage(newElemId, message) {
        let warningElem = prepareLoginWarningElem(newElemId, message);
        appendAfterElem(LOGIN_FIELD_ID, warningElem);
    }

    function removeLoginWarningMessage(warningElemId) {
        let warningElem = document.getElementById(warningElemId);

        if (warningElem !== null) {
            warningElem.remove();
        }
    }

    function prepareLoginWarningElem(newElemId, message) {
        let warningField = document.getElementById(newElemId);

        if (warningField === null) {
            let textMessage = document.createTextNode(message);
            warningField = document.createElement('span');

            warningField.setAttribute("id", newElemId);
            warningField.className = "warning-field";
            warningField.appendChild(textMessage);
        }
        return warningField;
    }

    function appendAfterElem(currentElemId, newElem) {
        let currentElem = document.getElementById(currentElemId);
        currentElem.insertAdjacentElement('afterend', newElem);
    }

    function isLoginAvailable() {
        return Promise.resolve(checkLoginAvailability().then(function (statusCode) {
            console.log(statusCode);
            if (statusCode === HTTP_STATUS.OK) {
                return false;

            } else if (statusCode === HTTP_STATUS.NOT_FOUND) {
                return true

            } else {
                throw "Unknown login availability status: " + statusCode;
            }
        }));
    }

    function checkLoginAvailability() {
        let loginInput = document.getElementById(LOGIN_FIELD_ID);
        let baseUrl = URL + "user/";
        let userUrl = baseUrl + loginInput.value;

        return Promise.resolve(fetch(userUrl, {method: GET}).then(function (resp) {
            return resp.status;
        }).catch(function (err) {
            return err.status;
        }));
    }
```

Powyższe części pokazują prawie cały plik `script.js`, ale dla poprawy jakości pracy z tym przykładem, poniżej zostanie umieszczony cały plik.

```js
document.addEventListener('DOMContentLoaded', function (event) {

    const GET = "GET";
    const POST = "POST";
    const URL = "https://pamiw2020registration.herokuapp.com/";

    const LOGIN_FIELD_ID = "login";

    var HTTP_STATUS = {OK: 200, CREATED: 201, NOT_FOUND: 404};

    prepareEventOnLoginChange();

    let registrationForm = document.getElementById("registration-form");

    registrationForm.addEventListener("submit", function (event) {
        event.preventDefault();

        console.log("Form submission stopped.");

        var n = event.srcElement.length;
        for (var i = 0; i < n; i++) {
            console.log(event.srcElement[i].value);
        }

        submitRegisterForm();
    });

    function submitRegisterForm() {
        let registerUrl = URL + "register";

        let registerParams = {
            method: POST,
            body: new FormData(registrationForm),
            redirect: "follow"
        };

        fetch(registerUrl, registerParams)
                .then(response => getRegisterResponseData(response))
                .then(response => displayInConsoleCorrectResponse(response))
                .catch(err => {
                    console.log("Caught error: " + err);
                });
    }

    function getRegisterResponseData(response) {
        let status = response.status;

        if (status === HTTP_STATUS.OK || status === HTTP_STATUS.CREATED) {
            return response.json();
        } else {
            console.error("Response status code: " + response.status);
            throw "Unexpected response status: " + response.status;
        }
    }

    function displayInConsoleCorrectResponse(correctResponse) {
        let status = correctResponse.registration_status;

        console.log("Status: " + status);

        if (status !== "OK") {
            console.log("Errors: " + correctResponse.errors);
        }
    }

    function prepareEventOnLoginChange() {
        let loginInput = document.getElementById(LOGIN_FIELD_ID);
        loginInput.addEventListener("change", updateLoginAvailabilityMessage);
    }

    function updateLoginAvailabilityMessage() {
        let warningElemId = "loginWarning";
        let warningMessage = "This login is already taken.";

        isLoginAvailable().then(function (isAvailable) {
            if (isAvailable) {
                console.log("Available login!");
                removeLoginWarningMessage(warningElemId);
            } else {
                console.log("NOT available login");
                showLoginWarningMessage(warningElemId, warningMessage);
            }
        }).catch(function (error) {
            console.error("Something went wrong while checking login.");
            console.error(error);
        });
    }

    function showLoginWarningMessage(newElemId, message) {
        let warningElem = prepareLoginWarningElem(newElemId, message);
        appendAfterElem(LOGIN_FIELD_ID, warningElem);
    }

    function removeLoginWarningMessage(warningElemId) {
        let warningElem = document.getElementById(warningElemId);

        if (warningElem !== null) {
            warningElem.remove();
        }
    }

    function prepareLoginWarningElem(newElemId, message) {
        let warningField = document.getElementById(newElemId);

        if (warningField === null) {
            let textMessage = document.createTextNode(message);
            warningField = document.createElement('span');

            warningField.setAttribute("id", newElemId);
            warningField.className = "warning-field";
            warningField.appendChild(textMessage);
        }
        return warningField;
    }

    function appendAfterElem(currentElemId, newElem) {
        let currentElem = document.getElementById(currentElemId);
        currentElem.insertAdjacentElement('afterend', newElem);
    }

    function isLoginAvailable() {
        return Promise.resolve(checkLoginAvailability().then(function (statusCode) {
            console.log(statusCode);
            if (statusCode === HTTP_STATUS.OK) {
                return false;

            } else if (statusCode === HTTP_STATUS.NOT_FOUND) {
                return true

            } else {
                throw "Unknown login availability status: " + statusCode;
            }
        }));
    }

    function checkLoginAvailability() {
        let loginInput = document.getElementById(LOGIN_FIELD_ID);
        let baseUrl = URL + "user/";
        let userUrl = baseUrl + loginInput.value;

        return Promise.resolve(fetch(userUrl, {method: GET}).then(function (resp) {
            return resp.status;
        }).catch(function (err) {
            return err.status;
        }));
    }
});
```

Przykłady kodu dla tego zadania są bardzo zbliżone do kodu z poprzedniej edycji jednak można w tutaj wskazać wyraźne różnice. Dlatego proszę nie próbować mieszać tych fragmentów kodu ze sobą, ponieważ moją pojawić się problemy w działaniu.

### HTML (registration.html)

Kod strony jest minimalistycznym kodem przedstawiającym formularz rejestracji. Proszę zwrócić uwagę na fakt, że pola formularza (atrybut `name`) mają nazwy odpowiadające polom wysyłanym w żądaniu rejestracji -- to nie jest przypadek. Dzięki temu możemy skorzystać z obiektu `FormData` w funkcji `submitRegisterForm()`.
Z poniższego kodu zostały również usunięte znaczniki `<br/>` występujące we wcześniejszej wersji.
W związku z tym proszę spróbować zmodyfikować klasę `form-row` (w narzędziach przeglądarki) dopisując do niej właściwość wyświetlania (display property) z wartością `inline-block` (końcowo: `display: inline-block;`). Co się wtedy stanie?

```html
<!Doctype html>
<html lang="pl">
    <head>
        <title>[POST-awa]</title>
        <meta charset="utf-8">
        <link href="styles/style.css" rel="stylesheet" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <script src="scripts/script.js"></script>
    </head>

    <body>
        <header>
            <h2>[POST-awa] Registration form</h2>
        </header>

        <nav>
            <ul>
                <li><a href="#">Info</a></li>
                <li><a href="#">Rejestracja</a></li>
                <li><a href="#">Kontakt</a></li>
            </ul>
        </nav>

        <section>

            <h3>Zarejestruj się</h3>

            <form id="registration-form" class="registration-form">

                <div class="form-row">
                    <label for="login" class="required">Login</label>
                    <input id="login" type="text" name="login"/>
                </div>

                <div class="form-row">
                    <label for="pesel" class="required">PESEL</label>
                    <input id="pesel" type="text" name="pesel"/>
                </div>

                <div class="form-row">
                    <label for="password" class="required">Hasło</label>
                    <input id="password" type="password" name="password"/>
                </div>

                <div class="form-row">
                    <label for="second_password" class="required">Powtórz hasło</label>
                    <input id="second_password" type="password" name="second_password"/>
                </div>

                <div class="form-row">
                    <input id="button-reg-form" type="submit" value="Rejestruj"/>
                </div>

            </form>

        </section>

        <footer>
            <p class="footer-inline left">&copy; 2020 Hans Castorp</p>
            <p class="footer-inline right">proudly powered by <img class="footer-img" src="images/vim-icon.png" alt="vim icon"/></p>
        </footer>

    </body>
</html>
```

### Heroku
Uruchomienie kodu odbywa się tak samo jak do tej pory.
Należy w katalogu z plikiem `Procfile` zainicjalizować repozytorium (`git init`), dodać wszystkie zmiany (`git add .`), stworzyć projekt Heroku (`heroku create`), wdrożyć zmiany na serwer (`git push heroku master`).


## Lab 4 Server Side
Dzisiejsze zajęcia poświęcimy na stworzenie aplikacji (lub tylko jej części) po stronie serwera, która umożliwi nam dodawanie („nadawanie”)) nowych przesyłek. Dodatkowo przygotujemy prosty kod klienta do weryfikacji tego działania.

### Drzewo katalogów

```
.
├── Dockerfile
└── package_register
    ├── model
    │   └── shipping.py
    ├── package_register_app.py
    ├── Procfile
    ├── requirements.txt
    ├── static
    │   └── styles
    │       └── style.css
    └── templates
        └── index.html

```

### Dockerfile

```
FROM python:3.7-slim

WORKDIR /package_register
COPY ./package_register /package_register

RUN pip install --trusted-host pypi.python.org -r requirements.txt

CMD ["python", "package_register_app.py"]
```

### requirements.txt

```
Flask
gunicorn
```

### Style (`style.css`)

```css
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    font-family: sans-serif;
}

.form-row {
    margin-top: 10px;
}
```

### App (`package_register_app.py`)

```python
from flask import Flask, request
from flask import render_template, jsonify
import logging
from model.shipping import *

app = Flask(__name__, static_url_path="")
log = app.logger

GET = "GET"
POST = "POST"

shipments = []

@app.before_first_request
def setup():
    log.setLevel(logging.DEBUG)


@app.route("/", methods=[GET])
def home():
    log.debug("Receive request for home.")

    return render_template("index.html", my_shipments = shipments)


@app.route("/shipment", methods=[POST])
def add_shipment():
    log.debug("Receive request for shipment.")
    form = request.form
    log.debug("Request form: {}".format(form))

    shipment = to_shipment(form)
    shipments.append(shipment)

    return jsonify({"shipment_title": shipment.get_title()}), 201


def to_shipment(request):
    product_name = request.get("product_name")
    sender = to_sender(request)
    recipient = to_recipient(request)

    return Shipping(product_name, sender, recipient)


def to_sender(request):
    sender_name = request.get("sender_name")
    sender_surname = request.get("sender_surname")

    return Person(sender_name, sender_surname)


def to_recipient(request):
    recipient_name = request.get("recipient_name")
    recipient_surname = request.get("recipient_surname")

    return Person(recipient_name, recipient_surname)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)
```

### Model ()

```python
import uuid

class Shipping:

    def __init__(self, product_name, sender, recipient):
        self.__id = uuid.uuid1()
        self.__product_name = product_name
        self.__sender = sender
        self.__recipient = recipient

    def get_title(self):
        return "{}_{}".format(self.__id, self.__product_name)


class Person:

    def __init__(self, name, surname):
        self.__name = name
        self.__surname = surname
```

### Formularz do wysyłania

```html
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>POST-awa</title>
        <meta charset="utf-8">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <header>
            <h2>POST-awa</h2>
        </header>
        <section>
            <h4>Lista nadanych paczek</h4>
            <ul>
                {% for shipment in my_shipments %}
                <li>{{shipment.get_title()}}</li>
                {% endfor %}
            </ul>
        </section>
        <section>
            <form method="POST" action="/shipment">

                <div class="form-row">
                    <label for="product_name">Nazwa produktu</label>
                    <input id="product_name" type="text" name="product_name">
                </div>

                <div class="form-row">                
                    <label for="sender_name">Imię nadawcy</label>
                    <input id="sender_name" type="text" name="sender_name">
                </div>

                <div class="form-row">
                    <label for="sender_surname">Nazwisko nadawcy</label>
                    <input id="sender_surname" type="text" name="sender_surname">
                </div>

                <div class="form-row">
                    <label for="recipient_name">Imię odbiorcy</label>
                    <input id="recipient_name" type="text" name="recipient_name">
                </div>

                <div class="form-row">
                    <label for="recipient_surname">Nazwisko odbiorcy</label>
                    <input id="recipient_surname" type="text" name="recipient_surname">
                </div>

                <div class="form-row">
                    <input type="submit" value="Wyślij">
                </div>
            </form>
        </section>
    </body>
</html>
```

### Procfile

```
web: gunicorn package_register_app:app
```

### Uruchomienie

Do uruchomienia aplikacji należy wykorzystać polecenia Docker-a i Heroku.

Dla Docker-a:

```sh
user@user:$ docker build . -t=pckg_register_app

```

```sh
user@user:$ docker run -p5000:5000 pckg_register_ap
```

Dla Heroku:

```sh
user@user:$ git init
```

```sh
user@user:$ git add .
```

```sh
user@user:$ git commit -m "All files has been added."
```

```sh
user@user:$ heroku create
```

```sh
user@user:$ git push heroku master
```

Do zweryfikowania logów aplikacji, proszę wykorzystać polecenie:

```sh
user@user:$ heroku logs --tail --ps web.1
```

### Zadania
1. Proszę rozszerzyć formularz o dodatkowe pola (np. numer telefonu, adres, rozmiar przesyłki, waga przesyłki).
2. Dane (wszystkie lub prawie wszystkie) przesłane w formularzu powinny wyświetlić się w tabeli nad formularzem (zamiast nienumerowanej listy z "nazwą przesyłki").
3. Proszę zmienić działanie formularza, aby po wysłaniu następowało przekierowanie na stronę formularza.
4. Proszę zweryfikować w logach aplikacji, że rzeczywiście następuje przekierowanie (jaki jest kod odpowiedzi na żądanie przekierowania?).
5. Proszę zabezpieczyć aplikację (po stronie serwera) przed przekazywaniem niepoprawnych danych.
6. Proszę odpowiedzieć, dlaczego na Heroku czasami nasze wysłane dane "znikają", a czasami "pojawiają się" (po odświeżeniu strony).

Do przekierowania strony proszę skorzystać z modułów: `redirect` oraz `url_for`. A w `add_shipment()` zamiast zwracać JSON, proszę zwrócić przekierowanie `return redirect(url_for("home"))`.

## Lab 5 Redis

Dzisiaj pochylimy się nad kolejnym etapem tworzenia naszej aplikacji.
Do budowania wykorzystamy bardziej zaawansowany mechanizm jakim jest narzędzie `docker-compose`. Umożliwia on (`docker-compose`) łatwiejsze zarządzanie bardziej złożonymi systemami (aplikacjami), co jeszcze dokładniej zobaczymy na kolejnych zajęciach.

Struktura plików naszej aplikacji jest przedstawiona poniżej.

```text
lab_5
├── docker-compose.yml
├── Dockerfile
└── waybill_generator
    ├── model
    │   └── waybill.py
    ├── requirements.txt
    ├── static
    │   └── styles
    │       └── style.css
    ├── templates
    │   └── index.html
    ├── waybill_files
    │   └── 
    └── waybill_generator_app.py
```

Większość plików / katalogów wygląda podobnie do tych, które już mogliśmy wcześniej zobaczyć.
Istotną różnicą jest plik docker-compose.yml zawierający złożoną konfigurację (jest wymagany, gdy chcemy korzystać z `docker-compose`).  Dodatkowo proszę zwrócić uwagę na istnienie pustego katalogu `waybill_files` – w nim będziemy przechowywać wygenerowane pliki (listy przewozowe).

W `requirements.txt` dodatkowo zostały umieszczone wpisy: `redis`, `fpdf`. Pierwszy z nich jest nam potrzebny do skorzystania z bazy danych `Redis`. Jest to baza typu klucz:wartość. My dzisiaj spróbujemy z niej skorzystać w celu zapisywania informacji o stworzonych (wygenerowanych) plikach PDF.

Plik `style.css` nie jest w żaden sposób zaskakujący – dodajemy go tylko po to, aby nasza testowa aplikacja była odrobinę przyjemniejsza do oglądania.

W `index.html` został umieszczony formularz z kilkoma polami do tworzenia listu przewozowego. Dodatkowo, po wysłaniu formularza, na serwerze powinien zostać wygenerowany plik PDF, a nazwa tego pliku powinna wyświetlić się pod formularzem (na liście wszystkich plików).

W `waybill.py` zapisana jest struktura naszych modeli (nadawca, odbiorca, list przewozowy). Proszę zwrócić uwagę na metody zapisujące adres – będzie trzeba je poprawić (uzupełnić prawdziwymi informacjami przesłanymi od klienta).
Proszę również zwrócić uwagę na metodę odpowiedzialną za tworzenie wynikowego pliku PDF.

Po przeanalizowaniu wszystkiego powyżej, zawartość `waybill_generator_app.py` nie powinna być trudna do zinterpretowania.

##### Uruchomienie

Stojąc w katalogu z `docker-compose.yml` wykonujemy polecenie: `docker-compose build`.
Ewentualnie można od razu napisać `docker-compose up` a przy kolejnych próbach budowania `docker-compose up --build`.

### Dockerfile

```sh
FROM python:3.7-slim

WORKDIR /waybill_generator
COPY ./waybill_generator /waybill_generator

RUN pip install --trusted-host pypi.python.org -r requirements.txt

CMD ["python", "waybill_generator_app.py"]
```

### docker-compose.yml

```sh
version: '3'
services:
  web:
    build: .
    ports:
      - "80:5000"
    volumes:
            - ./waybill_generator:/waybill_generator
    environment:
      FLASK_ENV: development
      FLASK_DEBUG: 1
  redis:
    image: "redis:alpine"
```

### requirements.txt

```sh
flask
redis
fpdf
```

### style.css

```css
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    font-family: 'Cambay', sans-serif;
}
```

### index.html

```html
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>Waybill generator</title>
        <meta charset="utf-8">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    </head>
    <body>
        <header>
            <h2>Sprawdź swój list przewozowy</h2>
        </header>
        <section>

            <form action="http://localhost/waybill" method="POST">

                <div>
                    <label for="sender_name">Imię nadawcy</label>
                    <input id="sender_name" type="text" name="sender_name">
                </div>

                <div>
                    <label for="sender_surname">Nazwisko nadawcy</label>
                    <input id="sender_surname" type="text" name="sender_surname">
                </div>

                <div>
                    <label for="recipient_name">Imię odbiorcy</label>
                    <input id="recipient_name" type="text" name="recipient_name">
                </div>

                <div>
                    <label for="recipient_surname">Nazwisko odbiorcy</label>
                    <input id="recipient_surname" type="text" name="recipient_surname">
                </div>

                <div>
                    <input type="submit" value="Wyślij">
                </div>

            </form>

            <ul>
                {% for file in my_files %}
                <li>{{ file }}</li>
                {% endfor %}
            </ul>

        </section>
    </body>
</html>
```

### waybill.py

```py
import uuid

from fpdf import FPDF


class Waybill:

    def __init__(self, sender, recipient):
        self.__sender = sender
        self.__recipient = recipient

    def generate_and_save(self, path="./"):
        pdf = FPDF()
        pdf.add_page()
        pdf.set_font("Arial", size=10)
        self.__add_table_to_pdf(pdf)

        filename = self.__generate_filename(path)
        pdf.output(filename)

        return filename

    def __add_table_to_pdf(self, pdf):
        n_cols = 2
        col_width = (pdf.w - pdf.l_margin - pdf.r_margin) / n_cols / 2
        font_size = pdf.font_size
        n_lines = 5

        pdf.cell(col_width, n_lines * font_size, "Sender", border=1)
        pdf.multi_cell(col_width, font_size, txt=self.__sender.str_full(), border=1)
        pdf.ln(0)
        pdf.cell(col_width, n_lines * font_size, "Recipient", border=1)
        pdf.multi_cell(col_width, font_size, txt=self.__recipient.str_full(), border=1)

    def __generate_filename(self, path):
        unique_filename = uuid.uuid4().hex

        return "{}{}.pdf".format(path, unique_filename)


class Person:

    def __init__(self, name: str, surname: str, address):
        self.__name = name
        self.__surname = surname
        self.__address = address

    def get_name(self):
        return self.__name

    def get_surname(self):
        return self.__surname

    def get_fullname(self):
        return "{} {}".format(self.__name, self.__surname)

    def get_address(self):
        return self.__address

    def str_full(self):
        return "{}\n{}".format(self.get_fullname(), self.__address.str_full())


class Address:

    def __init__(self, street: str, city: str, postal_code: str):
        self.__street = street
        self.__city = city
        self.__postal_code = postal_code

    def get_street(self):
        return self.__street

    def get_city(self):
        return self.__city

    def get_postal_code(self):
        return self.__postal_code

    def str_full(self):
        result = ""
        for field_value in self.__dict__.values():
            result += "\n{}".format(field_value)

        return result
```

### waybill_generator_app.py

```py
import logging
import os

import redis
from flask import Flask, request
from flask import render_template, jsonify, redirect, url_for
from flask import send_file
from model.waybill import *

app = Flask(__name__, static_url_path="")
log = app.logger
db = redis.Redis(host="redis", port=6379, decode_responses=True)

GET = "GET"
POST = "POST"

FILES_PATH = "waybill_files/"
PATH_AND_FILENAME = "path_and_filename"
FILENAMES = "filenames"


@app.before_first_request
def setup():
    log.setLevel(logging.DEBUG)


@app.route("/")
def show_waybills():
    files = db.hvals(FILENAMES)
    return render_template("index.html", my_files=files)


@app.route("/waybill/<string:waybill_hash>", methods=[GET])
def download_waybill(waybill_hash):
    log.debug("Received waybill download request [waybill_hash: {}].".format(waybill_hash))
    filename = waybill_hash + ".pdf"

    filepath = db.hget(filename, PATH_AND_FILENAME)

    if filepath is not None:
        try:
            return send_file(filepath, attachment_filename=filename)
        except Exception as e:
            log.error(e)

    return filename, 200


@app.route("/waybill", methods=[POST])
def add_waybill():
    log.debug("Received request to create a waybill.")
    form = request.form
    log.debug("Request form: {}.".format(form))

    waybill = to_waybill(form)
    save_waybill(waybill)

    return redirect(url_for("show_waybills"))


def to_waybill(form):
    sender = to_sender(form)
    recipient = to_recipient(form)

    return Waybill(sender, recipient)


def to_sender(form):
    name = form.get("sender_name")
    surname = form.get("sender_surname")
    address = to_sender_foo_address()

    return Person(name, surname, address)


def to_recipient(form):
    name = form.get("recipient_name")
    surname = form.get("recipient_surname")
    address = to_recipient_foo_address()

    return Person(name, surname, address)


def to_sender_foo_address():
    addr = Address("Nowogrodzka 84/86", "Warszawa", "02-018")
    return addr


def to_recipient_foo_address():
    addr = Address("A. Mickiewicza 49", "Warszawa", "01-625")
    return addr


def save_waybill(waybill):
    fullname = waybill.generate_and_save(FILES_PATH)
    filename = os.path.basename(fullname)

    db.hset(filename, PATH_AND_FILENAME, fullname)
    db.hset(FILENAMES, fullname, filename)

    log.debug("Saved waybill [fullname: {}, filename: {}].".format(fullname, filename))


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
```

### Zadania do wykonania

1. Proszę rozszerzyć pola formularza o brakujące pola adresu (dla nadawcy i odbiorcy): ulica, miasto, kod pocztowy, kraj.
2. Proszę zapisywać wszystkie dane z formularza (razem z krajem) w liście przewozowym (generowanym pliku PDF).
3. Nie należy dłużej korzystać z nazw metod zawierających `_foo_` -- należy zmienić nazwy tych metod lub całkowicie je usunąć, jeśli nie są dłużej potrzebne.
4. Po wysłaniu formularza, pod formularzem, ma być widoczna tabela (aktualnie jest lista nieuporządkowana) zawierająca dwie kolumny: nazwę pliku wygenerowanego listu przewozowego, przycisk do pobrania listu przewozowego (pliku PDF).
5. Do listu przewozowego należy dodać nowy wiersz zawierający dane wygenerowania tego pliku.
6. Proszę sprawdzić, czy formularz (i cała aplikacja) przyjmuje poprawnie polskie znaki diakrytyczne.
7. Jeżeli wszystkie powyższe punkty zostały wykonane, to można zapoznać się z tutorialem [Redis-a](https://try.redis.io
 

## Lab 6 Redis and session

1. Proszę uruchomić instancję bazy `Redis` poleceniem:

```sh
[user@user ~]$ docker run --rm --name redis -d -p 6379:6379 redis
```

2. Proszę uruchomić wiersz poleceń korzystający z uruchomionej przed chwilą instancji bazy `Redis`:

```sh
[user@user ~]$ docker exec -it redis redis-cli
```

3. Proszę, korzystając z poleceń: `sadd`, smembers stworzyć nieposortowany zbiór imion żeńskich (przynajmniej 6). Proszę wyświetlić wszystkie imiona.

4. Proszę usunąć trzy wybrane imiona korzystając z polecenia: `srem`.

5. Korzystając z polecenia `zadd` proszę stworzyć zbiór urzędników państwowych zawierający ich `Imię_Nazwisko` oraz `rok urodzenia`. Proszę wyświetlić tylko tych, którzy urodzili się w ciągu ostatnich 40 lat.

6. Proszę usunąć ze zbioru ostatniego urzędnika na liście.

7. Korzystając z polecenia `hset` proszę stworzyć użytkownika z polami: (imię, nazwisko, rok urodzenia, płeć). Proszę dodać 3 użytkowników.

### Aplikacja, redis, sesja

#### Struktura katalogów

```sh
.
├── docker-compose.yml
├── Dockerfile
└── session_app
    ├── requirements.txt
    ├── session_app.py
    ├── static
    │   ├── favicon.ico
    │   └── styles
    │       └── style.css
    └── templates
        ├── errors
        │   ├── 401.html
        │   └── 404.html
        ├── index.html
        ├── login.html
        └── secure
            └── secure-page.html
```

#### Dockerfile

```sh
FROM python:3.7-alpine
WORKDIR /session_app

ENV FLASK_APP session_app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 5000
ENV FLASK_ENV development

COPY ./session_app /session_app
RUN apk add --no-cache gcc musl-dev linux-headers openssl-dev libffi-dev
RUN pip install -r requirements.txt

CMD ["flask", "run", "--cert", "adhoc"]
```

#### docker-compose.yml

```sh
version: "3"
services:
  web:
    build: .
    ports: ["8080:5000"]
    volumes: [./session_app:/session_app]
  redis:
    image: "redis:alpine"
```

#### requirements.txt

```
flask
redis
pyopenssl
```

#### session_app.py

```python
from flask import Flask, render_template, request, make_response, abort
import redis
import hashlib

GET = "GET"
POST = "POST"
SESSION_ID = "session-id"

app = Flask(__name__, static_url_path="")
db = redis.Redis(host="redis", port=6379, decode_responses=True)


@app.route("/")
def home():
    return render_template("index.html")


@app.route("/login", methods=[GET, POST])
def login():
    if request.method == POST:
        username = request.form["username"].encode("utf-8")
        name_hash = hashlib.sha512(username).hexdigest()
        db.set(SESSION_ID, name_hash)
        response = make_response(render_template("index.html"))
        response.set_cookie(SESSION_ID, name_hash,
                            max_age=30, secure=True, httponly=True)
        return response
    else:
        return render_template("login.html")


@app.route("/secure", methods=[GET])
def secret_page():
    name_hash = request.cookies.get(SESSION_ID)

    if name_hash is not None:
        return render_template("secure/secure-page.html")
    else:
        abort(401)


@app.errorhandler(401)
def page_unauthorized(error):
    return render_template("errors/401.html", error=error)


@app.errorhandler(404)
def page_not_found(error):
    return render_template("errors/404.html", error=error)
```

#### style.css

```css
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    font-family: 'Cambay', sans-serif;
}
```

#### index.html

```html
<!DOCTYPE html>
<html lang="pl">

<head>
    <title>POSTawa -> Redis + Session</title>
    <meta charset="utf-8">
    <link href="/styles/style.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
</head>

<body>
    <header>
        <strong>[INDEX.HTML] Puść mą dłoń! Gnij schab, frytkę! Zwóź żel!</strong>
    </header>
    <section>
        <ul>
            <li><a href="{{ url_for('login') }}">Zaloguj</li>
            <li><a href="{{ url_for('secret_page')}}">Link do strony tylko dla zalogowanych</a></li>
        </ul>
    </section>
</body>

</html>
```

#### login.html

```html
<!DOCTYPE html>
<html lang="pl">

<head>
    <title>POSTawa -> Redis + Session</title>
    <meta charset="utf-8">
    <link href="/styles/style.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
</head>

<body>
    <header>
        <strong>[LOGIN.HTML] Znałem już pysk świń, chęć flot bądź gór</strong>
    </header>
    <section>
        <form action="https://localhost:8080/login" method="POST">
            <input type="text" name="username" />
            <input type="submit" value="Zaloguj" />
        </form>
    </section>
</body>

</html>
```

#### secure-page.html

```html
<!DOCTYPE html>
<html lang="pl">

<head>
    <title>POSTawa -> Redis + Session</title>
    <meta charset="utf-8">
    <link href="/styles/style.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
</head>

<body>
    <header>
        [<strong>SECURE</strong>_PAGE.HTML] Dość tych panagramów!
    </header>
    <section>
        <a href="{{ url_for('home') }}">Wróć do strony głównej</a>
    </section>
</body>

</html>
```

#### 401.html

```html
<!DOCTYPE html>
<html lang="pl">

<head>
    <title>[401] POSTawa -> Redis + Session</title>
    <meta charset="utf-8">
    <link href="/styles/style.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
</head>

<body>
    <section>
        <h1>401!</h1>
        <p>
            Nie masz dostępu do żądanego zasobu. Nie wiem, kim jesteś.
            <a href="{{ url_for('home') }}">Zawróć!</a>
        </p>
        <p>
            <strong>Szczegóły błędu:</strong>
            {{error}}
        </p>
    </section>
</body>

</html>
```

#### 404.html

```html
<!DOCTYPE html>
<html lang="pl">

<head>
    <title>[404] POSTawa -> Redis + Session</title>
    <meta charset="utf-8">
    <link href="/styles/style.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
</head>

<body>
    <section>
        <h1>404 :(</h1>
        <p>
            Nasz patrol zauważył Twoje zniknięcie. Ale nie wiemy, gdzie jesteś.
            <br />
            Nie możemy wysłać ekipy ratunkowej.
            <br />
            <a href="{{ url_for('home') }}">Zawróć, jeśli możesz.</a>
        </p>
        <p>
            <strong>Szczegóły błędu:</strong>
            {{error}}
        </p>
    </section>
</body>

</html>
```


## Lab 7 JWT

Żetony JWT (JSON Web Tokens) są kolejnym mechanizmem (obok `sessionID`) autoryzacji. Nie jest to jednak tak bezpieczny mechanizm, ale pozwala trochę zmienić logikę: żetonów można używać do potwierdzania, że ktoś już nas zidentyfikował, co oznacza, że możemy zostać dopuszczeni do zasobu. Wyróżnia się dwa typy żetonów `access token` oraz `refresh token` – w ramach dzisiejszego ćwiczenia będziemy wykorzystywać tylko pierwszy z nich.

Celem jest przygotowanie dwóch aplikacji: pierwsza z nich będzie służyła logowaniu (i przydzielaniu `JWT`), a druga posłuży do pobierania i wgrywania plików do naszej aplikacji.
Pobranie pliku będzie wymagało posiadania JWT.

### Struktura katalogów

```
lab_7
├── Docker
│   ├── files
│   │   └── Dockerfile
│   └── login
│       └── Dockerfile
├── docker-compose.yml
├── .env
└── POSTawa_app
    ├── const.py
    ├── files
    │   └── sdm.pdf
    ├── files_app.py
    ├── login_app.py
    ├── requirements.txt
    ├── static
    │   ├── favicon.ico
    │   └── styles
    │       └── style.css
    └── templates
        ├── index-files.html
        └── index.html
```

### requirements.txt

```text
flask
redis
pyopenssl
flask-jwt-extended
```

### Dockerfile (login)

```sh
FROM python:3.7-alpine
WORKDIR /POSTawa_app

ENV FLASK_APP login_app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 8880

COPY ./POSTawa_app /POSTawa_app

RUN apk add --no-cache gcc musl-dev linux-headers openssl-dev libffi-dev
RUN pip install -r requirements.txt

CMD ["flask", "run", "--cert", "adhoc"]
```

### Dockerfile (files)

```sh
FROM python:3.7-alpine
WORKDIR /POSTawa_app

ENV FLASK_APP files_app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 8881

COPY ./POSTawa_app /POSTawa_app

RUN apk add --no-cache gcc musl-dev linux-headers openssl-dev libffi-dev
RUN pip install -r requirements.txt

CMD ["flask", "run", "--cert", "adhoc"]
```


### docker-compose.yml

```sh
version: "3"
services:
  web-login:
    build:
      context: .
      dockerfile: ./Docker/login/Dockerfile
    ports: ["8080:8880"]
    volumes: [./POSTawa_app:/POSTawa_app]
    environment:
      FLASK_ENV: development
      LOGIN_JWT_SECRET: $LOGIN_JWT_SECRET
  web-files:
    build:
      context: .
      dockerfile: ./Docker/files/Dockerfile
    ports: ["8081:8881"]
    volumes: [./POSTawa_app:/POSTawa_app]
    environment:
      FLASK_ENV: development
      LOGIN_JWT_SECRET: $LOGIN_JWT_SECRET
  redis-db:
    image: "redis:alpine"
```


### .env

```sh
LOGIN_JWT_SECRET=tutaj_jakiś_sekretny_kod
```

### const.py

```python
GET = "GET"
POST = "POST"
SECRET_KEY = "LOGIN_JWT_SECRET"
TOKEN_EXPIRES_IN_SECONDS = 30
FILES_PATH = "files"
```

### login_app.py

```python
from flask import Flask, render_template, request
from flask_jwt_extended import JWTManager, create_access_token, jwt_required
from const import *
import redis
import os

app = Flask(__name__, static_url_path="")
db = redis.Redis(host="redis-db", port=6379, decode_responses=True)

app.config["JWT_SECRET_KEY"] = os.environ.get(SECRET_KEY)
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = TOKEN_EXPIRES_IN_SECONDS

jwt = JWTManager(app)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/login", methods=[POST])
def login():
    username = request.form["username"]
    access_token = create_access_token(identity=username)
    return {"access_token": access_token}


@app.route("/secret", methods=[GET])
@jwt_required
def secret():
    return {"secret_info": "herbata czarna myśli rozjaśnia"}
```

### files_app.py

```python
from flask import Flask, render_template, send_file, request
import logging
from const import *
from flask_jwt_extended import JWTManager, jwt_required
import redis
import os

app = Flask(__name__, static_url_path="")
db = redis.Redis(host="redis_db", port=6379, decode_responses=True)
log = app.logger

app.config["JWT_SECRET_KEY"] = os.environ.get(SECRET_KEY)
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = TOKEN_EXPIRES_IN_SECONDS

jwt = JWTManager(app)


def setup():
    log.setLevel(logging.DEBUG)


@app.route("/")
def index():
    return render_template("index-files.html")


@app.route("/secret-files", methods=[GET])
@jwt_required
def secret():
    return {"secret-info": "To jest strona z tajnymi listami przewozowymi w PDF.",
            "secret-message-base64": "V3J6ZXNpZcWEIC0gd3J6b3NlbSB6YWt3aXTFgiBsYXMgamVzaWXFhCAtIG1lbGFuY2hvbGlpIGN6YXMgeiBwYWxldMSFIGZhcmIgcHJ6eXdpYcWCIHdpYXRyIHphcGxhbWnFgiBjYcWCeSBwYXJrIGJvbMSFIG9jenkgb2QgdHljaCB3c3p5c3RraWNoIGJhcncgenJ1ZHppYcWCIG1jaGVtIHBvcm9zxYJ5IHBpZcWEIGplc2llxYQgLSBjb3JheiBrcsOzdHN6eSBrYcW8ZHkgZHppZcWEIHByenlzemVkxYJlbSBuYXN5Y2nEhyB3enJvayBuaW0gc3BhZG7EhSBsacWbY2llIHogZHJ6ZXcgbmltIHVtaWxrbmllIGNhxYJraWVtIHB0YWvDs3cgxZtwaWV3IHphbmltIHNwYWRuaWUgxZtuaWVnIG5pbSBvYmxlcGkgd3N6eXN0a28gxZtuaWVnIGxlxZtueW0gZHVrdGVtIGJlenN6ZWxlc3RuaWUgaWR6aWUgamVzaWXFhCBwZcWCbmEgYmFydyBtYSB3b2Fsa8SZIHogbWdpZcWCIGkgeiBiYWJpZWdvIGxhdGEgdHJlbiB3IHphbXnFm2xlbml1IHByemVjaG9kemkgendpZXduYSBqYWsgeiBvZ25pc2sgZHltLlRla3N0IHBvY2hvZHppIHogaHR0cHM6Ly93d3cudGVrc3Rvd28ucGwvcGlvc2Vua2Esc3RhcmVfZG9icmVfbWFsemVfc3R3byx3cnplc2llbi5odG1s"}


@app.route("/download-files/", methods=[GET])
@jwt_required
def download_file():
    try:
        full_filename = os.path.join(FILES_PATH, "sdm.pdf")
        return send_file(full_filename)
    except Exception as e:
        log.error("File not found :(")
        log.error(str(e))
        return {"message": "File not found... :("}, 404


@app.route("/upload-file", methods=[POST])
def upload_file():
    maybe_file = request.files["shipment_img"]
    save_file(maybe_file)
    return {"message": "Maybe saved the file."}


def save_file(file_to_save):
    if len(file_to_save.filename) > 0:
        path_to_file = os.path.join(FILES_PATH, file_to_save.filename)
        file_to_save.save(path_to_file)
    else:
        log.warn("Empty content of file!")
```

### style.css

```css
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    font-family: 'Cambay', sans-serif;
}
```

### index.html

```html
<!DOCTYPE html>
<html lang="pl">

<head>
    <title>POST-awa Login app (JWT)</title>
    <meta charset="utf-8">
    <link href="/styles/style.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
</head>

<body>
    <section>
        <p>Sprawdź swoje ciastka. Nie ma tam JWT?</p>
    </section>
</body>

</html>
```

### index-files.html

```html
<!DOCTYPE html>
<html lang="pl">

<head>
    <title>POST-awa Login app (files by JWT)</title>
    <meta charset="utf-8">
    <link href="/styles/style.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
</head>

<body>
    <section>
        <p>W tym miejscu możesz zaryzykować pobranie przykładowego pliku, jeśli masz żeton JWT.
            <a href="/download-files/" target="blank" download>Spróbujesz?</a>
        </p>
    </section>
</body>

</html>
```

### Zadania do wykonania

1. Należy uruchomić wszystkie skrypty (`docker-compose up`).
2. Proszę usunąć (wystarczy zmienić nazwę) plik `.env`, a następnie sprawdzić, czy aplikacja będzie działała.
3. Proszę stworzyć zmienną środowiskową `export env LOGIN_JWT_SECRET=123`. Jako wartość przypisywaną do zmiennej środowiskowej można wykorzystać wynik polecenia `openssl rand -base64 32`.
4. Proszę sprawdzić, czy po wykonaniu poprzednich kroków aplikacja działa poprawnie (nie jest wyświetlane ostrzeżenie o nieustawionej zmiennej środowiskowej po wykonaniu `docker-compose up`).
5. Proszę przetestować wszystkie end-point-y za pomocą `curl` oraz `Postman-a`. Analogicznie do przykładów:

```sh
[user@user ~]$ curl --insecure -X POST -F "username=Hans" https://localhost:8080/login
```
W odpowiedzi powinniśmy uzyskać jakiś `access_token` (według konfiguracji jest on ważny tylko przez 30 sekund).

```sh
{
  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MDU1NjcwMTksIm5iZiI6MTYwNTU2NzAxOSwianRpIjoiOTBmNzgxZDQtNDdmMi00OTJmLWE0MDEtOWQ1OGE2ZDY5NGUyIiwiZXhwIjoxNjA1NTY3MDQ5LCJpZGVudGl0eSI6IlBhd2VcdTAxNDIiLCJmcmVzaCI6ZmFsc2UsInR5cGUiOiJhY2Nlc3MifQ.-cr2Nwe5v33g2F1R5SZNC_6rwAWOmkb0gLPsQF-6Kz4"
}
```

Szybko wykonujemy próbę ściągnięcia pliku i zapisania go pod nazwą `sdm.pdf`.

```sh
[user@user ~]$ curl --insecure https://localhost:8081/download-files/ -o download.pdf -H "Authorization: Bearer eyJ0
eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MDU1NjcwMTksIm5iZiI6MTYwNTU2NzAxOSwianRpIjoiOTBmNzgxZDQtNDdmMi00OTJmLWE0MDEtOWQ1OGE2ZDY5NGUyIiwiZXhwIjoxNjA1NTY3MDQ5LCJpZGVudGl0eSI6IlBhd2VcdTAxNDIiLCJmcmVzaCI6ZmFsc2UsInR5cGUiOiJhY2Nlc3MifQ.-cr2Nwe5v33g2F1R5SZNC_6rwAWOmkb0gLPsQF-6Kz4"
```

Proszę zwrócić uwagę na konstrukcję nagłówka `Authorization`, która zawiera słowo kluczowe `Bearer` przed wartością JWT (otrzymaną w odpowiedzi z poprzedniego żądania).

6. Po przetestowaniu wszystkich adresów (nie tylko przykładowych z poprzedniego punktu) należy przygotować formularz (`html`) pozwalający na logowanie (w formularzu przekazujemy tylko `username`).
7. Proszę przygotować formularz pozwalający wgrywać pliki (zapisywać je na serwerze) – należy wykorzystać end-point `/upload-file`.
8. Proszę (wykorzystując JWT) wprowadzić niezbędne poprawki, aby ze strony `index-files.html` dało się pobrać plik klikając na: `Spróbujesz?`.


## Lab 8 Rest
(Poniższy materiał jest prawie w całości (99.9%) wzięty z materiałów z zeszłego roku.)
Zajęcia poświęcone są usługom sieciowym REST (Representational state transfer). Na przykładzie zamieszczonego kodu spróbujemy zobrazować, czym jest model dojrzałości Richardsona (Richardson Maturity Problem).

### Struktura katalogów
Struktura plików / katalogów, które trzeba przygotować, została przedstawiona poniżej.

```sh
lab_8
├── docker-compose.yml
├── Dockerfile
└── books_app
    ├── requirements.txt
    ├── rest_full_app.py
    └── src
        ├── dto
        │   ├── request
        │   │   ├── author_request.py
        │   │   └── book_request.py
        │   └── response
        │       └── paginated_book_response.py
        ├── exception
        │   └── exception.py
        └── service
            ├── author_service.py
            ├── book_service.py
            ├── entity
            │   ├── author.py
            │   └── book.py
            └── repositories
                ├── author_repository.py
                └── book_repository.py
```

### docker-compose.yml

```sh
version: "3"
services:
    web:
      build: .
      ports:
        - "8080:80"
      volumes:
        - ./books_app:/books_app
      environment:
        FLASK_ENV: development
    redis:
      image: "redis:alpine"
```

### Dockerfile

```sh
FROM python:3.7-alpine
WORKDIR /books_app
ENV FLASK_APP rest_full_app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 80
COPY ./books_app /books_app
RUN pip install -r requirements.txt
CMD ["flask", "run"]
```

### requirements.txt
Proszę zwrócić uwagę na dołączenie `flask-restplus`. Dzięki tej bibliotece nasza aplikacja zostanie zbudowana ze `Swagger-em` (popularnym narzędziem wspierającym: budowanie, dokumentowanie oraz testowanie API REST-owego). Dodatkowo została podana konkretna wersja `Werkzeug` ze względu na błąd w kompatybilności pomiędzy pozostałymi pakietami.

```txt
flask
flask-restplus
redis
Werkzeug==0.16.1
```

### rest_full_app.py (Python)

W tym istotnym fragmencie całej aplikacji należy zauważyć wykorzystanie biblioteki `flask-restplus`. To właśnie dzięki:

```python
api_app = Api(app = app, version = "...", title = "...", description = "...")
```

można skorzystać z domyślnej konfiguracji `Swagger-a`, którego interfejs webowy zostanie wystawiony pod adresem (`entrypoint`) zgodnym z naszą konfiguracją w plikach `Dockerfile` i `docker-compose.yml` (w przeglądarce należy odwiedzić: `http://localhost:8080`).

W tej przykładowej aplikacji zostały zdefiniowane trzy główne klasy określające grupę `endpoint-ów`: `hello_namespace` (Hello), `author_namespace` (Author), `book_namespace` (Book). Implementacja metod z nazwami odpowiadającymi czasownikom HTTP pozwala na jednoznaczne mapowanie ich przez kod, który automagicznie generuje webowy inerfejs Swagger-a.

Dodatkowo warto zwrócić uwagę na przykładowe wywołanie `api_app.model(...)`:

```python
new_author_model = api_app.model("Author model",
        {
            "name": fields.String(required = True, description = "...", help = "..."),
            "surname": fields.String(required = True, description = "...", help = "...")
        })
```

Dzięki takiej konfiguracji można określić strukturę parametrów oczekiwanych przez konkretny `endpoint`, używając dekoratora:

```python
@api_app.expect(new_author_model)
```

Poniżej jest już widoczny pełny kod źródłowy `rest_full_app.py`.

```python
from flask import Flask, request
from flask_restplus import Api, Resource, fields
from src.dto.request.book_request import BookRequest
from src.dto.request.author_request import AuthorRequest
from src.service.book_service import BookService
from src.service.author_service import AuthorService
from src.exception.exception import BookAlreadyExistsException, AuthorAlreadyExistsException, AuthorNotFoundByIdException

app = Flask(__name__)
api_app = Api(app = app, version = "0.1", title = "Books app API", description = "REST-full API for library")

hello_namespace = api_app.namespace("hello", description = "Info API")
author_namespace = api_app.namespace("author", description = "Author API")
book_namespace = api_app.namespace("book", description = "Book API")

START = "start"
LIMIT = "limit"

@hello_namespace.route("/")
class Hello(Resource):
    
    @api_app.doc(responses = {200: "OK, Hello World"})
    def get(self):
        return {
                "message": "Hello, World!"
        }

@author_namespace.route("/")
class Author(Resource):

    def __init__(self, args):
        super().__init__(args)
        self.author_service = AuthorService()

    new_author_model = api_app.model("Author model",
            {
                "name": fields.String(required = True, description = "Author name", help = "Name cannot be blank"),
                "surname": fields.String(required = True, description = "Author surname", help = "Surname cannot be null") 
            })

    @api_app.expect(new_author_model)
    def post(self):
        try:
            author_req = AuthorRequest(request)
            saved_author_id = self.author_service.add_author(author_req)
            
            result = {"message": "Added new author", "save_author_id": saved_author_id}

            return result

        except AuthorAlreadyExistsException as e:
            author_namespace.abort(409, e.__doc__, status = "Could not save author. Already exists", statusCode= 409)


@book_namespace.route("/<int:id>")
class Book(Resource):

    @api_app.doc(responses = {200: "OK", 400: "Invalid argument"},
            params = {"id": "Specify book Id"})
    def get(self, id):
        try:

            return {
                    "message": "Found book by id: {0}".format(id)
            }

        except Exception as e:
            book_namespace.abort(400, e.__doc__, status = "Could not find book by id", statusCode = "400")
    
    @api_app.doc(responses = {200: "OK", 400: "Invalid argument"},
            params = {"id": "Specify book Id to remove"})
    def delete(self, id):
        try:

            return {
                    "message": "Removed book by id: {0}".format(id)
            }

        except Exception as e:
            book_namespace.abort(400, e.__doc__, status = "Could not remove book by id", statusCode = "400")

@book_namespace.route("/list")
class BookList(Resource):

    def __init__(self, args):
        super().__init__(args)
        self.book_service = BookService()
        self.author_service = AuthorService()

    new_book_model = api_app.model("Book model",
            {
                "title": fields.String(required = True, description = "Book title", help = "Title cannot be null", example = "Bieguni"),
                "year": fields.Integer(required = True, description = "Year of publication", help = "Year cannot be null", example = "2007"),
                "author_id": fields.Integer(required = True, description = "Author's Id ", help = "Author's Id cannot be null")
            })

    @api_app.param(START, "The data will be returned from this position.")
    @api_app.param(LIMIT, "The max size of returned data.")
    @api_app.doc(responses = {200: "OK"})
    def get(self):
        start = self.parse_request_arg_or_zero(request, START, "0")
        start = max(1, start)
        limit = self.parse_request_arg_or_zero(request, LIMIT, "50")

        paginated_book_response = self.book_service.get_paginated_books_response(start, limit)
        
        return paginated_book_response.get_json(request.base_url)

    def parse_request_arg_or_zero(self, request, param, default_value):
        val = request.args.get(param, default_value)
        val = int(val) if val.isdigit() else 0
        return val

    @api_app.expect(new_book_model)
    def post(self):
        try:
            book_req = BookRequest(request)
            author = self.author_service.get_author_by_id(book_req.author_id)
            saved_book_id = self.book_service.add_book(book_req)

            result = {"message": "Added new book", "saved_book_id": saved_book_id}

            return result

        except KeyError as e:
            book_namespace.abort(400, e.__doc__, status = "Could not save new book", statusCode = "400")

        except BookAlreadyExistsException as e:
            book_namespace.abort(409, e.__doc__, status = "Could not save new book. Already exists", statusCode = "409")

        except AuthorNotFoundByIdException as e:
            book_namespace.abort(404, e.__doc__, status = "Could not save new book. Author (by id) does not exist.", statusCode = "404")
```

### DTO (Data Transfer Object)
Poniższe trzy kody źródłowe zostały stworzone do obiektowej reprezentacji otrzymywanych / zwracanych danych. Przy użyciu takich obiektów wygodniej jest przekazywać informacje (dane) pomiędzy kolejnymi (zdalnymi) serwisami całej aplikacji. W przypadku prezentowanej aplikacji taka struktura może wydawać się nadmiarowa, jednak ze względów praktycznych została zaimplementowana.

### dto/request/author_request.py (Python)

```python
class AuthorRequest:
    def __init__(self, request):
        self.name = request.json["name"]
        self.surname = request.json["surname"]

    def __str__(self):
        return "name: {0}, surname: {1}".format(self.name, self.surname)
```

### dto/request/book_request.py (Python)

```python
class BookRequest:
    def __init__(self, request):
        self.author_id = request.json["author_id"]
        self.title = request.json["title"]
        self.year = request.json["year"]

    def __str__(self):
        return "author_id: {0}, title: {1}, year: {2}".format(self.author_id, self.title, self.year)
```

### dto/response/paginated_book_response.py (Python)

```python
from flask import jsonify
import json

class PaginatedBookResponse:
    def __init__(self, books, start, limit, count):
        self.books = []

        for book in books:
            self.books.append(book.__dict__)

        self.start = start
        self.limit = limit
        self.current_size = len(books)
        self.count = count

    def get_json(self, url):
        if self.start <= 1:
            previous_url = ""
        else:
            start_previous = max(1, self.start - self.limit)
            previous_url = "{0}?start={1}&limit={2}".format(url, start_previous, self.limit)

        if self.start + self.limit > self.count:
            next_url = ""
        else:
            start_next = self.start + self.limit
            next_url = "{0}?start={1}&limit={2}".format(url, start_next, self.limit)

        return {
                "books": self.books,
                "start": self.start,
                "limit": self.limit,
                "current_size": self.current_size,
                "count": self.count,
                "previous": previous_url,
                "next": next_url
                }
```

### exception/exception.py (Python)
Poniższe klasy mogą sprawiać wrażenie, że nic się w nich nie dzieje. I to prawda! Ale dzięki jednoznacznych nazw klas wyjątków, łatwiej będzie rozróżnić błędy generowane podczas działania aplikacji.

```python
class BookAlreadyExistsException(Exception):
    pass

class AuthorAlreadyExistsException(Exception):
    pass

class AuthorNotFoundByIdException(Exception):
    pass
```

### service/author_service.py
Proszę zwrócić uwagę, że w poniższym kodzie (kolejny raz) wykorzystywane są metody do logowania informacji o przebiegu działania aplikacji (`app.logger.debug("...")`). Być może warto, jak było wcześniej, wprowadzić zmienną `log = app.logger`, aby skrócić zapis.

```python
from flask import Flask
from src.service.repositories.author_repository import AuthorRepository
from src.exception.exception import AuthorNotFoundByIdException

app = Flask(__name__)

class AuthorService:

    def __init__(self):
        self.author_repo = AuthorRepository()

    def add_author(self, author_req):
        app.logger.debug("Adding author...")
        author_id = self.author_repo.save(author_req)
        app.logger.debug("Added author (id: {0})".format(author_id))
        return author_id

    def get_author_by_id(self, author_id):
        app.logger.debug("Getting author by id: {0}.".format(author_id))
        author = self.author_repo.find_by_id(author_id)

        if author == None:
            raise AuthorNotFoundByIdException("Not found author by id: {0}".format(author_id))

        app.logger.debug("Got author by id: {0}".format(author_id))
        return author
```

### book_service.py (Python)

```python
from flask import Flask
from src.service.repositories.book_repository import BookRepository
from src.dto.response.paginated_book_response import PaginatedBookResponse

app = Flask(__name__)

class BookService:

    def __init__(self):
        self.book_repo = BookRepository()

    def add_book(self, book_req):
        app.logger.debug("Adding book...")
        book_id = self.book_repo.save(book_req)
        app.logger.debug("Added book (id: {0})".format(book_id))
        return book_id
    
    def get_paginated_books_response(self, start, limit):
        app.logger.debug("Getting paginated books (start: {0}, limit: {1})".format(start, limit))
        n_of_books = self.book_repo.count_all()
        
        books = self.book_repo.find_n_books(start, limit)

        books_response = PaginatedBookResponse(books, start, limit, n_of_books)

        app.logger.debug("Got paginated books (start: {0}, limit: {1}, count: {2}, current_size: {3})".format(start, limit, n_of_books, len(books)))
        return books_response
```

### @classmethod
W poniżej prezentowanych dwóch plikach (`book.py`, `author.py`) wykorzystano `pie-decorator`, czyli składnię dekoratorów (z symbolem`@`) znaną z innych języków programwania (np. Javy). W tym przypadku `@classmethod` pełni rolę oznaczenia metody, którą można rozumieć jako metodę statyczną -- do jej wywołania nie jest potrzebna konkretna instancja klasy (obiekt).

### service/entity/book.py (Python)

```python
import json

class Book:
    def __init__(self, id, author_id, title, year):
        self.id = id
        self.author_id = author_id
        self.title = title
        self.year = year

    @classmethod
    def from_json(cls, data):
        return cls(**data)
```

### service/entity/author.py (Python)

```python
import json

class Author:
    def __init__(self, id, name, surname):
        self.id = id
        self.name = name
        self.surname = surname

    @classmethod
    def from_json(cls, data):
        return cls(**data)
```

### service/repositories/author_repository.py (Python)

```python
from flask import Flask
import redis
import json

from ...service.entity.author import Author
from ...exception.exception import AuthorAlreadyExistsException

app = Flask(__name__)

AUTHOR_COUNTER = "author_counter"
AUTHOR_ID_PREFIX = "author_"

class AuthorRepository:

    def __init__(self):
        self.db = redis.Redis(host = "redis", port = 6379, decode_responses = True)
        if self.db.get(AUTHOR_COUNTER) == None:
            self.db.set(AUTHOR_COUNTER, 0)
    
    def save(self, author_req):
        app.logger.debug("Saving new author: {0}.".format(author_req))
        author = self.find_by_names(author_req.name, author_req.surname)

        if author != None:
            raise AuthorAlreadyExistsException("Author (name: \"{0}\", surname: \"{1}\") already exists".format(author_req.name, author_req.surname))

        author = Author(self.db.incr(AUTHOR_COUNTER), author_req.name, author_req.surname)

        author_id = AUTHOR_ID_PREFIX + str(author.id)
        author_json = json.dumps(author.__dict__)

        self.db.set(author_id, author_json)

        app.logger.debug("Saved new author: (id: {0}).".format(author.id))
        return author.id

    def find_by_names(self, name, surname):
        n = int(self.db.get(AUTHOR_COUNTER))

        for i in range(1, n + 1):
            author_id = AUTHOR_ID_PREFIX + str(i)

            if not self.db.exists(author_id):
                continue

            author_json = self.db.get(author_id)
            author = Author.from_json(json.loads(author_json))

            if author.name == name and author.surname == surname:
                return author

        return None

    def find_by_id(self, author_id_to_find):
        n = int(self.db.get(AUTHOR_COUNTER))

        for i in range(1, n + 1):
            author_id = AUTHOR_ID_PREFIX + str(i)

            if not self.db.exists(author_id):
                continue

            author_json = self.db.get(author_id)
            author = Author.from_json(json.loads(author_json))

            if author.id == author_id_to_find:
                return author

        return None
```

### service/repositories/book_repository.py (Python)

```python
from flask import Flask
import redis
import json

from ...service.entity.book import Book
from ...exception.exception import BookAlreadyExistsException

app = Flask(__name__)

BOOK_COUNTER = "book_counter"
BOOK_ID_PREFIX = "book_"

class BookRepository:
    
    def __init__(self):
        self.db = redis.Redis(host = "redis", port = 6379, decode_responses = True)
        if self.db.get(BOOK_COUNTER) == None:
            self.db.set(BOOK_COUNTER, 0)
    
    def save(self, book_req):
        app.logger.debug("Saving new book: {0}.".format(book_req))
        book = self.find_book_by_title(book_req.title)

        if book != None:
            raise BookAlreadyExistsException("Book title \"{0}\" already exist.".format(book_req.title))
            
        book = Book(self.db.incr(BOOK_COUNTER), book_req.author_id, book_req.title, book_req.year)
        
        book_id = BOOK_ID_PREFIX + str(book.id)
        book_json = json.dumps(book.__dict__)
        
        self.db.set(book_id, book_json)
        
        app.logger.debug("Saved new book: (id: {0}).".format(book.id))
        return book.id

    def find_book_by_title(self, title):
        n = int(self.db.get(BOOK_COUNTER))

        for i in range(1, n + 1):
            book_id = BOOK_ID_PREFIX + str(i)
            
            if not self.db.exists(book_id):
                continue
            
            book_json = self.db.get(book_id)
            book = Book.from_json(json.loads(book_json))
            
            if book.title == title:
                return book

        return None

    def count_all(self):
        app.logger.debug("Starting counting all books")
        n = int(self.db.get(BOOK_COUNTER))

        n_of_books = 0

        for i in range(1, n + 1):
            book_id = BOOK_ID_PREFIX + str(i)

            if self.db.exists(book_id):
                n_of_books += 1

        app.logger.debug("Counted all books (n: {0})".format(n_of_books))
        return n_of_books

    def find_n_books(self, start, limit):
        app.logger.debug("Finding n of books (start: {0}, limit: {1}".format(start, limit))
        n = int(self.db.get(BOOK_COUNTER))

        books = []
        counter = 1
        
        for i in range(1, n + 1):
            book_id = BOOK_ID_PREFIX + str(i)

            if not self.db.exists(book_id):
                continue

            if counter < start:
                counter += 1
                continue

            book_json = self.db.get(book_id)
            book = Book.from_json(json.loads(book_json))
            books.append(book)

            if len(books) >= limit:
                break

        app.logger.debug("Found {0} books.".format(len(books)))
        return books
```

### Ważne
Przedstawione powyżej fragmenty kodu źródłowego, ze wskazaniem na metody do paginacji tabeli z książkami, charakteryzują (niepełną) usługę sieciową REST na trzecim poziomie dojrzałości Richardsona. Leonard Richardson jest twórcą modelu, w którym rozróżnia się cztery poziomy.

1. (Level 0) Jeżeli nasz serwis udostępnia tylko jeden endpoint, który służy za punkt wejścia i wyjścia do wszelkiej komunikacji -- czyli pełni rolę semantycznie zbliżoną do RPC (remote procedure call) -- to można powiedzieć, że jest to najniższy poziom w modelu dojrzałości Richardsona. W ten sposób mogą działać usługi sieciowe typu SOAP. Najczęściej dane wysyłane są jako XML, a wiadomość zwrotna (zależna od stanu zasobów) również odsyłana jest w tej formie. Kolejne zapytanie (ze zmienioną strukturą XML-a) wysyłane jest pod ten sam `endpoint`, a wiadomość zwrotna (z prawdopodobnie inną strukturą XML w porównaniu do poprzedniej) również wysyłana jest jako XML.
2. (Level 1) Na tym poziomie dojrzałości serwis udostępnia kilka `entry-point-ów` / `endpoint-ów` w zależności od zasobów, po które chcemy sięgnąć. Odnosząc się do prezentowanego powyżej przykładu kodu źródłowego, można powiedzieć, że jeżeli będziemy mieli osobne adresy (URI) do zapytań związanych z książkami i osobne adresy (URI) do zapytań związanych z autorami książek, a wszystkie zapytania będą odbywały się bez rozróżnienia czasowników HTTP (czyli będziemy używali jednego typu żądań, np. POST), to serwis będzie na pierwszym poziomie w modelu dojrzałości Richardsona.
3. (Level 2) Kolejny poziom odróżnia się od poprzedniego poprzez czytelność wykonywanych operacji. Jeżeli komunikacja odbywa się przez HTTP, to należy wykorzystywać czasowniki HTTP (GET, POST, PUT, DELETE, ...) dla rozróżnienia typu operacji podczas odwoływania się po konkretny zasób (pod konkretne URI). Oprócz czasowników HTTP należy również wykorzystywać kody odpowiedzi do sygnalizowania statusu żądania (jeżeli operacja powiodła się, to kod `2xx` (np. `200`) jest oczekiwany, a jeżeli coś się nie udało, to należy to zgłosić wykorzystując jeden z wielu dostępnych kodów odpowiedzi: `4xx`, `5xx`).
4. (Level 3) Poziom często określany jako REST-full API a kojarzony z HATEOAS (Hypertext As The Engine Of Application State). Oznacza to, że jeżeli sięgniemy po jakiś zasób, to w informacji zwrotnej dostaniemy również wiadomość, co możemy dalej z tym zasobem zrobić. W przypadku kodu związanego z paginacją książek w odpowiedzi wysyłane były informacje pod jakim adresem znajdują się (jeśli istnieją): poprzednia strona i następna strona z listą książek. Od serwisu z trzecim poziomem dojrzałości Richardsona można oczekiwać dobrze udokumentowanego, czytelnego API z jednoznacznym opisem używanych kodów odpowiedzi.

### Zadania do zrealizowania w ramach zajęć
1. Proszę uruchomić załączone fragmenty kodu (`docker-compose up`).
2. Proszę sprawdzić wszystkie początkowo istniejące end-pointy za pomocą `Postman-a`.
3. Proszę rozszerzyć / uzupełnić kod w taki sposób, aby możliwe było: dodawanie autora, dodawanie książki (powiązana z autorem), usuwanie książki (po `ID` książki), pobieranie listy wszystkich książek.
4. Proszę odpowiedzieć na pytanie: na którym poziomie modelu dojrzałości Richardsona (RMM) znajduje się opisane rozwiązanie?


## Lab 9 Responsywne

### Struktura plików

```sh
lab_9
├── docker-compose.yml
├── Dockerfile
└── static_html
    ├── images
    │   ├── arrow-down-circle.svg
    │   ├── arrow-down-right-circle.svg
    │   ├── buddenbrookowie.jpg
    │   ├── czarni.jpg
    │   ├── daleko-od-wawelu.jpg
    │   ├── sztuczna-inteligencja-nieludzka-arcyludzka.jpg
    │   └── ulisses.jpg
    └── index.html
```

### Dockerfile

```sh
FROM nginx

COPY ./static_html /usr/share/nginx/html
```

### docker-compose.yml

```sh
version: "3"
services:
  static_web:
    build: .
    ports: ["8080:80"]
    volumes: [./static_html:/usr/share/nginx/html]
```

### index.html

```html
<!doctype html>
<html lang="pl">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        crossorigin="anonymous">
    <title>POST-awa app -- bootstrap test</title>
</head>

<body>

    <div class="container">
        <div class="row">
            <!-- miejsce na przyciski -->
        </div>

        <hr class="col-11" />

        <div class="row">
            <div class="card col-6 col-md-4 col-lg-3">
                <!-- karta Buddenbrookowie -->
            </div>
            <div class="card col-6 col-md-4 ml-lg-auto col-lg-3">
                <!-- karta Czarni -->
            </div>
            <div class="card col-6 col-md-4 ml-lg-auto col-lg-3">
                <!-- karta Daleko od Wawelu -->
            </div>
        </div>

        <div class="row">
            <div class="card col-6 col-md-4 col-lg-3">
                <!-- karta Sztuczna inteligencja nieludzka arcyludzka -->
            </div>
            <div class="card col-6 ml-auto col-md-4 col-lg-3">
                <!-- karta Ulisses -->
            </div>
        </div>

        <hr class="col-11" />

        <div class="row">
            <!-- Ciekawe czy w górach lodowych<br />w schroniskach lodowe są szybki? -->
            <!-- Czy w górach lodowych lodowe góralki -->
        </div>

        <div class="row">
            <!-- lodowe sprzedają oscypki? -->
            <!-- Ciekawe czy w górach lodowych <br />rosną lodowe lasy? -->
        </div>

        <div class="row">
            <!-- Czy w górach lodowych lodowi górale -->
            <!-- mają lodowe... szałasy. -->
        </div>

        <hr class="col-11" />

    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        crossorigin="anonymous"></script>
</body>

</html>
```

### Zadania do zrealizowania w ramach zajęć
1. Proszę uruchomić szkielet strony `index.html` (`docker-compose up`).
2. Proszę, korzystając z `Google Lighthouse`, sprawdzić ocenę kilku wybranych stron (m.in. strony wydziałowej).
3. Proszę uzupełnić pierwszy `div` z klasą `row` czterema przyciskami (`button`), które
      - dla najmniejszej szerokości będą wyświetlały się pod sobą;
      - dla kolejnej szerokości będą wyświetlały się w układzie 2x2;
      - dla szerokości `large` będą wyświetlały się obok siebie bez przerwy;
      - dla szerokości `extra large` będą wyświetlały się obok siebie z równym odstępem (każdy ma szerokość dwóch kolumn).
4. Proszę wyświetlić 5 okładek książek (korzystając z komponentu `card`) w taki sposób, że dla najmniejszej szerokości książki są wyświetlane po dwie w rzędzie, a dla największej -- po trzy w rzędzie. W rzędzie środkowym powinna wystąpić luka na książkę.
5. Proszę wprowadzić zmianę do rozwiązania z zadania 4., aby nie występowała luka pomiędzy książką 3. i 4.
6. Proszę zmodyfikować ostatnie trzy elementy `div` z klasą `row` w taki sposób, aby dla najmniejszej szerokości wszystkie były wyświetlane jeden pod drugim, a dla każdej większej szerokości występowały tylko po dwa przyciski w każdym wierszu i każdej kolumnie (tak samo, jak pokazywałem w trakcie zajęć).
 

## Lab 10 Progresywne

### Zadania do zrealizowania w ramach zajęć

#### Część pierwsza
1. Proszę sprawdzić, korzystając z narzędzi developerskich, jak zachowują się wybrane strony, gdy przeglądarka („urządzenie”) straci połączenie z Internetem.
2. Wykorzystując materiały z poprzednich zajęć, proszę przygotować stronę `index.html` zawierającą tabelę oraz formularz. Strona powinna dobrze się wyświetlać na ekranach typowych dla telefonów, tabletów, monitorów.
3. W tabeli należy umieścić wybrane dane udostępnione przez https://jsonplaceholder.typicode.com/users lub https://gorest.co.in/public-api/users.
4. Formularz powinien umożliwiać dodawanie nowych użytkowników, czyli powinien zawierać takie same pola (wszystkie?) jak te prezentowane w tabeli.
5. Do dodawania użytkownika należy wykorzystać https://gorest.co.in/public-api/users (z metodą POST).
6. Proszę wyświetlać komunikat (wybrany komponent z `Bootstrap-a`), gdy nie udaje się poprawnie wysłać żądania.
7. Proszę przygotować stronę `offline.html`, która powinna się wyświetlać, gdy urządzenie / aplikacja straci połączenie z Internetem.

#### Część druga

Zbudujemy przykład PWA -- nasza aplikacja powinna być responsywna (w ramach ćwiczenia należy to poprawić), "instalowalna", działająca bez połączenia z Internetem (my będziemy wyświetlać komunikat).

Należy stworzyć strukturę katalogów zgodnie z poniżej wyświetlonym schematem.

```sh
lab_10
├── docker-compose.yml
├── Dockerfile
└── static_html
    ├── error.html
    ├── images
    │   ├── delivery_man_icon-128-128.png
    │   ├── delivery_man_icon-16-16.png
    │   ├── delivery_man_icon-256-256.png
    │   ├── delivery_man_icon-32-32.png
    │   ├── delivery_man_icon-48-48.png
    │   ├── delivery_man_icon-512-512.png
    │   ├── delivery_man_icon-64-64.png
    │   ├── delivery_man_icon-90-90.png
    │   └── delivery_man_icon.png
    ├── index.html
    ├── manifest.webmanifest
    ├── offline.html
    ├── scripts
    │   └── script.js
    ├── service-worker.js
    └── styles
        └── style.css
```

Proszę zwrócić uwagę, że mamy 3 strony `html`: `error`, `offline`, `index`. Strona `index.html` jest naszą stroną główną. Strona `offline.html` powinna wyświetlać się, gdy nie będziemy mogli nawiązać połączenia z Internetem. Strona `error.html` jest przeznaczona na sygnalizację innych błędów niż błąd połączenia (podczas ładowania strony) -- prawdopodobnie nikt z nas nie spowoduje jej wyświetlenia.

W katalogu `images` są zdjęcia wygenerowane przez serwis [Firebase](https://app-manifest.firebaseapp.com/). 
Zdjęcie główne (ikona dla naszej przykładowej aplikacji) zostało [pobrane z Internetu](https://adioma.com/icons/moving-books).

##### Dockerfile

```sh
FROM nginx

COPY ./static_html /usr/share/nginx/html
```


##### docker-compose.yml

```sh
version: "3"
services:
  static_web:
    build: .
    ports: ["8080:80"]
    volumes: [./static_html:/usr/share/nginx/html]
```

##### manifest.webmanifest
Pliku `manifestu` zawiera informację o tym, której ikony użyć w zależności od "sytuacji" (m.in. chodzi o urządzenie, które będzie chciało nam wyświetlić ikonę). Określamy również, co ma się stać (jaki adres strony ma się otworzyć) po uruchomieniu naszej aplikacji (zmierzamy do tego, że zainstalujemy naszą aplikację na urządzeniu: komputerze, tablecie, telefonie). Do tego celu wykorzystujemy wartość przypisaną do `start_url`. Możemy również określić zakres naszych stron, które mają wyświetlać się w ramach uruchomionej aplikacji (ustalamy to wartością przypisaną do `scope`).
Wartością `display` mówimy, jak ma zostać wyświetlona nasza aplikacja (wyglądać jak niezależna aplikacja -- `standalone`; wyświetlać się jak strona w przeglądarce -- `browser`; zajmować cały dostępny ekran -- `fullscreen`).

```sh
{
  "name": "POST-awa delivery system",
  "short_name": "POST-awa",
  "description": "Delivery system for customers",
  "icons": [
    {
      "src": "/images/delivery_man_icon-48-48.png",
      "type": "image/png",
      "sizes": "48x48"
    },
    {
      "src": "/images/delivery_man_icon-90-90.png",
      "type": "image/png",
      "sizes": "90x90"
    },
    {
      "src": "/images/delivery_man_icon-256-256.png",
      "type": "image/png",
      "sizes": "256x256"
    },
    {
      "src": "/images/delivery_man_icon-512-512.png",
      "type": "image/png",
      "sizes": "512x512"
    }
  ],
  "theme_color": "#2196f3",
  "background_color": "#2196f3",
  "display": "standalone",
  "scope": "/",
  "start_url": "/"
}
```

##### script.js

W pliku `script.js` niewiele się dzieje oprócz rejestracji elementu `serviceWorker`. Po udanym procesie powinien wyświetlić się komunikat w konsoli przeglądarki. Należy zwrócić uwagę na podaną ścieżkę do pliku `service-worker.js`, który musi znajdować się w głównym katalogu aplikacji.

```js
window.addEventListener('load', () => {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker
      .register('../service-worker.js')
      .then(function () { console.log("ServiceWorker correctly registered"); });
  }
});
```

##### service-worker.js

Drugim głównym plikiem całej aplikacji (obok `manifestu`) jest plik `service-worker.js`, ponieważ to w nim ustalamy m.in to, co ma się dziać, gdy ktoś nas odłączy od Internetu.

Na początku pliku definiujemy stałe:

- `CACHE_NAME` -- nazwa naszego kontenera do przechowywania zapamiętanych elementów;
- `OFFLINE_URL` -- adres strony, która ma się wyświetlać, gdy stracimy połączenie z Internetem;
- `ERROR_URL` -- adres strony wyświetlanej, gdy coś (innego niż błąd ładowania strony) pójdzie nie tak.

Zdarzenie `install` jest wykorzystywane do zapisywania wszystkich stron podczas pierwszego ładowania. Proszę zwrócić uwagę na wartość `cache: "reload"` oraz na wywołanie `self.skipWaiting()`. Proszę znaleźć w [dokumentacji](https://developer.mozilla.org), jakie jest znaczenie tego kodu.

Dodatkowo w tym kodzie zamiast jawnych wywołań `Promise.then({...})` został użyty zamiennik `async() / await`. Proszę sprawdzić w [dokumentacji](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Async_await), jaka jest różnica między tymi zapisami i jak należy je rozumieć. Szczególną uwagę proszę zwrócić na temat zalet i wad `async() / await`.

W ostatniej części (`fetch`) następuje decyzja o tym, co zostanie wyświetlone użytkownikowi. Jeżeli mamy połączenie z Internetem, to wykonamy kod wewnątrz `try{}` -- czyli wyświetlimy oczekiwaną stronę. Jeżeli nie mamy połączenia z Internetem, to sprawdzamy, czy błąd powstał podczas ładowania strony (np. podczas przechodzenia na kolejną stronę), czy może jest on wynikiem innej sytuacji. W pierwszym przypadku wyświetlimy zapisaną w pamięci stronę `Offline`, a w drugim -- `Error`.

```js
const CACHE_NAME = "POSTawa_cache";
const OFFLINE_URL = "offline.html";
const ERROR_URL = "error.html";

self.addEventListener("install", (event) => {
  event.waitUntil((async () => {
    let cache = await caches.open(CACHE_NAME);
    await cache.add(new Request(OFFLINE_URL, { cache: "reload" }));
    await cache.add(new Request(ERROR_URL, { cache: "reload" }));
  })());
  self.skipWaiting();
});

self.addEventListener("activate", (event) => {
  event.waitUntil(self.clients.claim());
});

self.addEventListener("fetch", (event) => {
  event.respondWith((async () => {
    try {
      let responseFromNetwork = await fetch(event.request);
      return responseFromNetwork;

    } catch (error) {
      console.log("Caught error during fetch", error);
      let cache = await caches.open(CACHE_NAME);
      let cachedResponse;

      if (event.request.mode === "navigate") {
        cachedResponse = await cache.match(OFFLINE_URL);
      } else {
        cachedResponse = await cache.match(ERROR_URL);
      }
      return cachedResponse;
    }
  })());
});
```

##### style.css

```css
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    font-family: sans-serif;
}
```

##### index.html

Proszę zwrócić uwagę na kod `<meta name="viewport"...` -- jest on związany z tym, jak wyświetli się nasza strona na ekranie (jak będzie widoczna na małym ekranie urządzenia mobilnego).

```html
<!DOCTYPE html>
<html lang="pl">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="manifest" href="/manifest.webmanifest" />
  <link rel="stylesheet" href="/styles/style.css" />
  <script src="/scripts/script.js"></script>

  <title>POST-awa (PWA example)</title>
</head>

<body>
  <h1>Gotowy na wysłanie paczki?</h1>

  <p>
    A próbowałeś kiedyś w trybie offline?
    <br />
    Naciśnij <strong>F5</strong>, gdy odłączysz się od sieci.
  </p>
</body>

</html>
```

##### error.html

```html
<!DOCTYPE html>
<html lang="pl">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>POST-awa (connection error)</title>
</head>

<body>
  <h1>Mamy problem :(</h1>

  <p>
    Użytkowniku, Towarzyszu podróży, dotarłeś do miejsca, w którym nie powinieneś się nigdy znaleźć.
    <br>
    Spróbuj zawrócić i wezwij pomoc.
  </p>
</body>

</html>
```

##### offline.html

```html
<!DOCTYPE html>
<html lang="pl">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>POST-awa (offline)</title>
</head>

<body>
  <h1>Niestety straciliśmy z Tobą łączność</h1>

  <p>
    Przypuszczamy, że nie jesteś podłączony do Internetu.
    <br />
    Zweryfikuj plotek prawdziwość i wróć do nas. Bez Ciebie nie idziemy!
  </p>
  <p>
    Naciśnij <strong>F5</strong>, gdy będziesz gotowy na powrót.
  </p>
</body>

</html>
```

##### Zadania do zrealizowania w drugiej części
1. W ramach ćwiczenia (po uruchomieniu) proszę sprawdzić, czy kod dla zdarzenia `activate` ma jakiś odczuwalny wpływ na działanie aplikacji.
2. Proszę usunąć (lub zakomentować na chwilę) element `<meta name="viewport"...` w pliku `index.html`. Proszę na nowo załadować stronę na małym ekranie (np. korzystając w przeglądarce z narzędzi deweloperskich). Jaka jest zmiana? Widać różnicę?
3. Proszę sprawdzić w `Google Chrome` -> `Application` -> `Manifest`, czy udało się załadować plik `manifestu`.
4. Proszę sprawdzić w `Google Chrome` -> `Application` -> `Service Workers`, czy wszystko zostało poprawnie załadowane (`Status`). Proszę przeładować stronę po przejściu w tryb `Offline` (w zakładce `Service Workers`).
5. Proszę spróbować zainstalować stronę jako aplikację na urządzeniu (np. na komputerze) -- w Google Chrome w pasku adresu (z prawej strony) powinna być widoczna ikona instalacji. Jeżeli ikona nie jest widoczna, to należy sprawdzić komunikaty w zakładce: `Application` -> `Manifest` (być może jesteśmy w trybie `Incognito`).

Powyższy przykład (z kilkoma koniecznymi usprawnieniami) został uruchomiony na `Heroku`. Proszę spróbować uruchomić [stronę](https://postawa-mobile.herokuapp.com/) przez urządzenie mobilne. Proszę zainstalować aplikację (korzystając z ikony wyświetlanej po załadowaniu strony), a następnie proszę spróbować uruchomić aplikację (z / bez "trybu samolotowego"). Proszę przeładować stronę (w moim przypadku trzeba przeciągnąć po ekranie palcem w dół). Powinna wyświetlić się informacja o braku połączenia z Internetem (`error.html`).

## Lab 11 Gniazda

### Struktura katalogów

```sh
├── chat_app
│   ├── requirements.txt
│   ├── static
│   │   ├── scripts
│   │   │   └── script.js
│   │   └── styles
│   │       └── style.css
│   ├── templates
│   │   └── index.html
│   └── web_socket_chat_app.py
├── docker-compose.yml
├── Dockerfile
```

### Dockerfile

```sh
FROM python:3.7-alpine
WORKDIR /chat_app

COPY ./chat_app /chat_app

RUN apk add --no-cache gcc musl-dev linux-headers
RUN pip install -r requirements.txt

CMD ["python", "web_socket_chat_app.py"]
```

### docker-compose.yml

```sh
version: "3"
services:
  web:
    build: .
    ports: ["8080:80"]
    volumes: [./chat_app:/chat_app]
  redis:
    image: "redis:alpine"
```

### requirements.txt

```txt
flask
flask-socketio
eventlet
```

### index.html

```html
<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="utf-8">
    <link href="/styles/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/3.0.4/socket.io.min.js"></script>
    <script src="/scripts/script.js"></script>
    <title>POSTawa chat -- web sockets example</title>
</head>

<body>
    <section>
        <p>Tutaj mamy wstawić nasz chat z trzema (czterema?) okienkami.</p>
    </section>
</body>

</html>
```

### style.css

```css
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
}
```

### script.js

```js
const ROOM_RED_ID = "red_id";
const ROOM_GREEN_ID = "green_id"
const ROOM_BLUE_ID = "blue_id"
const ROOM_IDS = [ROOM_RED_ID, ROOM_GREEN_ID, ROOM_BLUE_ID];

document.addEventListener("DOMContentLoaded", function (event) {

    var ws_uri = "http://" + document.domain + ":" + location.port;
    socket = io.connect(ws_uri);

    socket.on("connect", function () {
        console.log("Correctly connected to the chat");
    });

    socket.on("joined_room", function (message) {
        console.log("Joined to the room ", message);
    });

    socket.on("chat_message", function (data) {
        console.log("Received new chat message:", data);
    });

    socket.on("chat_global_message", function (data) {
        let msg = data.message;
        console.log("Received new global message:", msg);
    });

    joinIntoAllRooms();
    sendMsgRed("Wysyłam coś na RED room.");
    leaveGreenRoom();
    leaveBlueRoom();
    sendMsgGreen("Wysyłam wiadomość do GREEN room");
    sendMsgBlue("Kolejna wiadomość? Teraz BLUE room.");

    joinIntoRoom(ROOM_BLUE_ID);
    sendMsgBlue("Druga próbna wiadomość do BLUE");
    sendMsgGreen("Druga wiadomość do GREEN");

    joinIntoRoom(ROOM_GREEN_ID);
    sendGlobalMessage("Ta wiadomość powinna dotrzeć do wszystkich");
});

function joinIntoAllRooms() {
    ROOM_IDS.forEach(room_id => {
        joinIntoRoom(room_id);
    });
}

function joinIntoRoom(room_id) {
    useragent = navigator.userAgent;
    socket.emit("join", { useragent: useragent, room_id: room_id });
}

function leaveGreenRoom() {
    leaveRoom(ROOM_GREEN_ID);
}

function leaveBlueRoom() {
    leaveRoom(ROOM_BLUE_ID);
}

function leaveRoom(room_id) {
    console.log("Leave room: ", room_id);
    useragent = navigator.userAgent;
    socket.emit("leave", { useragent: useragent, room_id: room_id });
}

function sendMsgRed(text) {
    sendMessage(ROOM_RED_ID, text);
}

function sendMsgGreen(text) {
    sendMessage(ROOM_GREEN_ID, text);
}

function sendMsgBlue(text) {
    sendMessage(ROOM_BLUE_ID, text);
}

function sendMessage(room_id, text) {
    data = { room_id: room_id, message: text };
    socket.emit("new_message", data);
}

function sendGlobalMessage(text) {
    data = { message: text };
    socket.emit("new_global_message", data);
}
```

### web_scoket_chat_app.py

```python
from flask import Flask, render_template
from flask_socketio import SocketIO, join_room, leave_room, emit, send

app = Flask(__name__, static_url_path="")
socket_io = SocketIO(app)

USER_AGENT = "useragent"
ROOM_ID = "room_id"
MESSAGE = "message"


@app.route("/")
def index():
    return render_template("/index.html")


@socket_io.on("connect")
def handle_on_connect():
    app.logger.debug("Connected -> OK")
    emit("connection response", {"data": "Correctly connected"})


@socket_io.on("disconnect")
def handle_on_disconnect():
    app.logger.debug("Disconnected -> Bye")


@socket_io.on("join")
def handle_on_join(data):
    useragent = data[USER_AGENT]
    room_id = data[ROOM_ID]
    join_room(room_id)
    emit("joined_room", {"room_id": room_id})
    app.logger.debug("Useragent: %s added to the room: %s" %
                     (useragent, room_id))


@socket_io.on("leave")
def handle_on_leave(data):
    useragent = data[USER_AGENT]
    room_id = data[ROOM_ID]
    leave_room(room_id)
    app.logger.debug("Useragent: %s is no longer in the room: %s" %
                     (useragent, room_id))


@socket_io.on("new_message")
def handle_new_message(data):
    app.logger.debug(f"Received data: {data}.")
    emit("chat_message", {MESSAGE: data[MESSAGE]}, room=data[ROOM_ID])


@socket_io.on("new_global_message")
def handle_new_global_message(data):
    app.logger.debug(f"Received global message data: {data}.")
    emit("chat_global_message", {MESSAGE: data[MESSAGE]})


if __name__ == '__main__':
    socket_io.run(app, host="0.0.0.0", port=80, debug=True)
```

## Lab 12 Autoryzacja

*(Poniższy materiał jest prawie w całości zaczerpnięty z materiałów 2019.)*

Kolejne zajęcia są poświęcone mechanizmom zewnętrznej autoryzacji, które (oprócz autoryzacji) pomagają w udostępnianiu innym systemom / aplikacjom / usługom / serwisom danych wymagających poświadczenia przed otrzymaniem do nich dostępu. Najpopularniejszym quasi-standardem (quasi-protokołem) takiej komunikacji stał się `OAuth 2.0`. Popularność i mnogość zastosowań tego fragmentu współczesnej technologii jest tak duża, że trudno dzisiaj byłoby wskazać poważną organizację świadczącą usługi teleinformatyczne, która nie korzystałaby z rozwiązań kryjących się pod enigmatyczną osłoną `OAuth 2.0`. W skrócie chodzi o to, aby zezwolić jednej usłudze (klientowi) na dostęp do innej usługi (do danych).
Z punktu widzenia użytkownika końcowego takie podejście jest wygodne, bo m.in. to właśnie dzięki temu nie trzeba w każdym serwisie / portalu zakładać nowego konta (tworzyć nowego loginu, hasła), choć trzeba potwierdzić znajomość z regulaminem, ponieważ można skorzystać z istniejącego już systemu, który w naszym imieniu poświadczy, że dane autoryzacyjne (a często też identyfikacyjne), których używamy, są prawdziwe.

### Cel zajęć

W trakcie zajęć zostanie postawionych kilka zadań, których wykonanie nie sprawi, że `OAuth` zostanie całkowicie poskromiony i przestanie skrywać tajemnice, ale istnieje duża szansa, że po wykonaniu tych zadań, quasi protokół `OAuth 2.0` przestanie być tak bardzo tajemniczy, jak może sprawiać wrażenie na początku.

### Auth0.com

Do efektywnej pracy podczas tych zajęć zostanie wykorzystany serwis `auth0.com`, w którym -- dla poprawnego przebiegu zadań -- należy założyć konto (i uwaga -- można skorzystać z innych systemów poświadczających nasze uprawnienia). Po udanej rejestracji konta w `auth0.com` należy stworzyć nową aplikację. W tym celu należy postępować zgodnie z poniższymi krokami.

1. Z lewego menu trzeba wybrać `APIs`.
2. W widoku z nagłówkiem `APIs` należy kliknąć na `Create API`.
3. Uzupełnienie pól `Name` i `Identifier` (pozostawiamy algorytm `RS256`) jest proponowanym działaniem zbliżającym nas do kolejnego punktu. Nie ma większego znaczenia, jakimi wartościami zostaną uzupełnione dwa pierwsze pola.
4. Z lewego menu trzeba wybrać `Applications`.
5. W widoku z nagłówkiem `Applications` należy kliknąć na `Create Application`.
6. W oknie tworzenia aplikacji należy uzupełnić pole `Name` oraz wybrać (proponowany typ) `Machine to Machine Applications`.
7. Po kliknięciu na przycisk potwierdzający nasz wybór, należy z listy rozwijanej wybrać API stworzone w poprzednich krokach.
8. Po przejściu do zakładki `Settings` (w ramach tej konkretnej aplikacji) należy uzupełnić pola w następujący sposób. Do `Allowed Callback URLs` należy wpisać `http://localhost:8080/callback`. Do pola `Allowed Logout URLs` należy wpisać `http://localhost:8080/logout_info`. Zmiany należy zachować poprzez kliknięcie na `Save changes`. Nad tym dużym przyciskiem przyciskiem (`Save changes`) jest znacznie mniejszy (`Show Advanced Settings`) -- po kliknięciu należy przejść do zakładki `Grant Types` i wybrać: `Authorization Code` oraz `Refresh Token` (opcjonalnie -- nie będziemy z tego teraz korzystać). **Ważne:** `Client Credentials` powinien pozostać zaznaczony.
9. Jeżeli do tej pory wszystko zostało wykonane poprawnie, to z menu bocznego można wybrać `APIs` a następnie wskazać dodane `API`. Przechodząc do zakładki `Test` można sprawdzić, że wywołanie curl rzeczywiście zwrócić oczekiwany `access_token`.
10. Istotne jest aby z zakładki `Settings` dla wskazanej aplikacji (z okna `Applications`) zanotować `Domain`, `Client ID`, `Client Secret`.
11. Kolejne kroki są już związane z implementacją rozwiązania -- kod jest umieszczony poniżej.

### Zmienne środowiskowe

Dla poprawnej (i bezpiecznej pracy), należy stworzyć dwie zmienne środowiskowe (uzupełniając je odpowiednimi wartościami).

```sh
user@user:~$ openssl rand -base64 23
>>> vp8uYVthb8QYgfJOzk4Y4UCezPwRKs8=
user@user:~$ export env LAB_AUTH_SECRET=vp8uYVthb8QYgfJOzk4Y4UCezPwRKs8
user@user:~$ export env OAUTH_CLIENT_SECRET=UWAGA!!!_Tutaj_należy_wpisać_wartość_CLIENT_SECRET_zanotowaną_z_auth.com
```

### Drzewo plików i katalogów

Należy stworzyć strukturę odpowiadającą tej poniżej przedstawionej.

```sh
lab_12
├── auth0_app
│   ├── const_config.py
│   ├── oauth_client_app.py
│   ├── requirements.txt
│   ├── static
│   │   └── styles
│   │       └── style.css
│   └── templates
│       ├── index.html
│       └── secure
│           ├── logout.html
│           └── secure-page.html
├── docker-compose.yml
├── Dockerfile
```

### docker-compose.yml

Proszę zwrócić uwagę na mapowanie zmiennych środowiskowych: `LAB_AUTH_SECRET`, `OAUTH_CLIENT_SECRET`.

```sh
version: "3"
services:
  web:
    build: .
    ports: ["8080:80"]
    volumes: [./auth0_app:/auth0_app]
    environment:
      FLASK_ENV: development
      LAB_AUTH_SECRET: $LAB_AUTH_SECRET
      OAUTH_CLIENT_SECRET: $OAUTH_CLIENT_SECRET
```

### Dockerfile

```sh
FROM python:3.7-alpine

WORKDIR /auth0_app

ENV FLASK_APP oauth_client_app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 80

COPY ./auth0_app /auth0_app

RUN apk add --no-cache gcc musl-dev linux-headers openssl-dev libffi-dev
RUN pip install -r requirements.txt

CMD ["flask", "run"]
```

### requirements.txt

```txt
flask
requests
authlib
```

### const_config.py

Ten plik należy uzupełnić indywidualnymi wartościami z `auth0.com`.
Pod `OAUTH_BASE_URL` należy wpisać wartość `Domain`, a pod `OAUTH_CLIENT_ID` należy wstawić `Client ID`.

Przykładowy plik został przedstawiony poniżej.

```python
import os

OAUTH_BASE_URL = "https://zawadzp4-pamiw.eu.auth0.com"
OAUTH_ACCESS_TOKEN_URL = OAUTH_BASE_URL + "/oauth/token"
OAUTH_AUTHORIZE_URL = OAUTH_BASE_URL + "/authorize"
OAUTH_CALLBACK_URL = "http://localhost:8080/callback"
OAUTH_CLIENT_ID = "viMlnTAoMXvxM3whYg3MQ1JrwOKh95Wm"
OAUTH_CLIENT_SECRET = os.environ.get("OAUTH_CLIENT_SECRET")
OAUTH_SCOPE = "openid profile"
SECRET_KEY = os.environ.get("LAB_AUTH_SECRET")
NICKNAME = "nickname"
```

### oauth\_client\_app.py

Należy przeanalizować poniższy kod. Proszę zwrócić uwagę na import wszystkich wartości z `const_config.py`. Należy również przeanalizować działanie `authorization_required` -- funkcja wykorzystuje mechanizm dekorowania kodu, czyli nim wykona docelowy kod, to najpierw dokona sprawdzenia warunku (`if NICKNAME not in session:`).
Warto również zwrócić uwagę na fakt, że ten przykład korzysta z innego mechanizmu zarządzania sesją. (W ramach bieżących zajęć można z niego skorzystać. Ale nie jest on dozwolony w trakcie wykonywania kolejnych kamieni milowych projektu zaliczeniowego.)
Z punktu widzenia `OAuth` kluczowa jest funkcja `oauth_callback`. Kiedy (w ramach mechanizmu `OAuth`) zostanie ona wywołana? Proszę odpowiedzieć na to pytanie.

```python
from flask import Flask, render_template, session, url_for, redirect
from authlib.integrations.flask_client import OAuth
from functools import wraps
from const_config import *

app = Flask(__name__, static_url_path="")
app.secret_key = SECRET_KEY
oauth = OAuth(app)

auth0 = oauth.register(
    "lab-12-auth0-2020",
    api_base_url=OAUTH_BASE_URL,
    client_id=OAUTH_CLIENT_ID,
    client_secret=OAUTH_CLIENT_SECRET,
    access_token_url=OAUTH_ACCESS_TOKEN_URL,
    authorize_url=OAUTH_AUTHORIZE_URL,
    client_kwargs={"scope": OAUTH_SCOPE})


def authorization_required(fun):
    @wraps(fun)
    def authorization_decorator(*args, **kwds):
        if NICKNAME not in session:
            return redirect("/login")

        return fun(*args, **kwds)

    return authorization_decorator


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/login")
def login():
    return auth0.authorize_redirect(
        redirect_uri=OAUTH_CALLBACK_URL,
        audience="")


@app.route("/logout_info")
def logout_info():
    return render_template("./secure/logout.html")


@app.route("/logout")
def logout():
    url_params = "returnTo=" + url_for("logout_info", _external=True)
    url_params += "&"
    url_params += "client_id=" + OAUTH_CLIENT_ID

    session.clear()
    return redirect(auth0.api_base_url + "/v2/logout?" + url_params)


@app.route("/secure")
@authorization_required
def secure():
    return render_template("./secure/secure-page.html")


@app.route("/callback")
def oauth_callback():
    auth0.authorize_access_token()
    resp = auth0.get("userinfo")
    nickname = resp.json()[NICKNAME]

    session[NICKNAME] = nickname

    return redirect("/secure")
```

### style.css

```css
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    font-family: sans-serif;
}
```

### index.html

```html
<!Doctype html>
<html lang="pl">

<head>
    <title>POST-awa OAuth -- OAuth (2.0) example</title>
    <meta charset="utf-8">
    <link href="/styles/style.css" rel="stylesheet" type="text/css">
</head>

<body>
    To powinna być strona logowania (a działa?)...
</body>

</html>
```

### secure-page.html

```html
<!Doctype html>
<html lang="pl">

<head>
    <title>POST-awa OAuth -- OAuth (2.0) example -- Secure page</title>
    <meta charset="utf-8">
    <link href="/styles/style.css" rel="stylesheet" type="text/css">
</head>

<body>
    Tutaj dostęp mają tylko zalogowani użytkownicy.
</body>

</html>
```

### logout.html

```html
<!Doctype html>
<html lang="pl">
    <head>
        <title>POST-awa OAuth -- OAuth (2.0) example -- Logout</title>
        <meta charset="utf-8">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        Nastąpiło bezpieczne wylogowanie.
    </body>
</html>
```

### Uruchomienie

Po stworzeniu wszystkich plików, całość powinno się zbudować i uruchomić w przeglądarce (`localhost:8080`).

```sh
ubuntu:~$ docker-compose up --build
```

Strony nie mają przycisków nawigacyjnych, ale modyfikując bezpośrednio wartość w pasku adresu, proszę wykonać trzy podstawowe operacje. Proszę się zalogować (`http://localhost:8080/login`). Proszę potwierdzić, że nastąpiło przekierowanie na `http://localhost:8080/secure`. Proszę sprawdzić, że przy próbie przejścia (ponownego wpisania) na `http://localhost:8080/login` następuje automatyczne przekierowanie na stronę `secure`. Proszę się wylogować (w pasku adresu należy wpisać: `localhost:8080/logout`) i potwierdzić, że nastąpiło przekierowanie na stronę `localhost:8080/logout_info`.

Powyższy przykład pokazuje poprawną (acz niepełną) próbę wykorzystania quasi-protokołu `OAuth 2.0`.

### Zadania do wykonania w ramach zajęć
1. Proszę zweryfikować logi aplikacji w serwisie `auth0.com` (zakładka `Logs`).
2. Proszę zmodyfikować w `auth0.com` wartości przypisane pod `Grant Types` (proszę odznaczyć `Client Credentials`), a następnie proszę przejść do okna testowania API. Co się stało? Dlaczego? Proszę odpowiedzieć na te "ogólne" pytania.
3. Proszę za pomocą logger-a (`app.logger.debug()`) sprawdzić, jakie informacje są przekazywane w `userinfo`. Proszę zmodyfikować wartość `OAUTH_SCOPE` o dodatkową wartość `email`, a następnie proszę sprawdzić, czy dane przekazywane w `userinfo` uległy zmianie.
4. Proszę odszukać miejsce (w `auth0.com`), w którym są zdefiniowane wszystkie endpoint-y udostępnione w ramach `OAuth`. Jest to przydatna informacja, z której m.in. korzysta się podczas ustalania adresu pobierania żetonów autoryzacyjnych.


## Lab 13 Kolejki

#### Struktura katalogów

```sh
lab_13
├── docker-compose.yml
├── Dockerfile
└── rabbit_example_app
    ├── rabbitmq_app.py
    └── requirements.txt
```

#### docker-compose.yml

```sh
version: "3"
services:
  web:
    build: .
    ports: ["8080:80"]
    volumes: [./rabbit_example_app:/rabbit_example_app]
    environment:
      FLASK_ENV: development

  rabbitmq:
    hostname: lab13_rabbitmq
    image: rabbitmq:latest
    ports: ["5672:5672"]
```

#### Dockerfile

```sh
FROM python:3.7-alpine
WORKDIR /rabbit_example_app
COPY ./rabbit_example_app /rabbit_example_app

ENV FLASK_APP rabbitmq_app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 80

RUN apk add --no-cache gcc musl-dev linux-headers
RUN pip install -r requirements.txt

CMD ["flask", "run"]
```

#### requirements.txt

```txt
flask
pika
```

#### rabbitmq_app.py

```python
from flask import Flask, request
import pika
import time

GET = "GET"
POST = "POST"
QUEUE_NAME = "msg_queue"
DELIVERY_MODE_PERSISTENT = 2
DELAY = 2
NUM_OF_TASK_PER_CONSUMER = 1

app = Flask(__name__, static_url_path="")


def open_channel():
    rabbit_mq_connection = pika.BlockingConnection(
        pika.ConnectionParameters(host="rabbitmq"))
    channel = rabbit_mq_connection.channel()
    channel.queue_declare(queue=QUEUE_NAME, durable=True)
    return channel


@app.route("/")
def index():
    return "Hello, World!", 200


@app.route("/add-message", methods=[POST])
def add_message_on_queue():
    message = request.form.get("msg")

    channel = open_channel()
    channel.basic_publish(
        exchange="",
        routing_key=QUEUE_NAME,
        body=message,
        properties=pika.BasicProperties(delivery_mode=DELIVERY_MODE_PERSISTENT))

    app.logger.debug(message)
    return "Added message", 201


def callback_mq(channel, method, properties, body):
    app.logger.debug("Inside callback_mq()")
    time.sleep(DELAY)
    app.logger.info("Message from queue %s" % body)
    app.logger.info("DONE")
    channel.basic_ack(delivery_tag=method.delivery_tag)


@app.route("/start-consuming-messages", methods=[GET])
def consume_messages():
    channel = open_channel()
    channel.basic_qos(prefetch_count=NUM_OF_TASK_PER_CONSUMER)
    channel.basic_consume(queue=QUEUE_NAME, on_message_callback=callback_mq)
    channel.start_consuming()
```

#### Zadania do wykonania w trakcie zajęć
1. Proszę uruchomić przykładowy kod.
2. Proszę zweryfikować ustawienia `DELIVERY_MODE_PERSISTENT=2` oraz `durable=True`. Czy to chroni nasze komunikaty, gdy serwer nagle „zgaśnie”?
3. Proszę zmodyfikować kod w taki sposób, aby rozdzielić od siebie konsumenta i producenta.
4. Proszę uruchomić zmodyfikowany kod w taki sposób, aby dało się uruchomić dwóch (i / lub trzech) konsumentów.
5. Proszę sprawdzić, czy kolejka działa w trybie  `round-robin` (domyślnie powinna tak pracować)?  Czy jednak wartość `NUM_OF_TASK_PER_CONSUMER` zmienia to działanie?
