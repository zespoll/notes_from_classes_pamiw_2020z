from flask import Flask, render_template, make_response

app = Flask(__name__, static_url_path="")


@app.route("/")
def home():
    return render_template("index.html")


@app.route("/offline")
def offline():
    return render_template("offline.html")


@app.route("/error")
def error():
    return render_template("error.html")


@app.route("/service-worker.js")
def service_worker():
    return app.send_static_file("service-worker.js")
