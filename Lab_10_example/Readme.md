### Lab 10 Progressive Web Application

W tym miejscu zamieszczam zmodyfikowany przykład dla aplikacji progresywnej.
Całość uruchamiamy przez `docker-compose up`, ale do prawidłowego działania wymagane jest posiadanie certyfikatów (do łączenia protokołem HTTPS).
Generowaniem certyfikatów zajmiemy się w przyszłości (na kolejnych zajęciach), dlatego dopuszczalne dzisiaj jest testowanie swojego rozwiązania poprzez wyłączenie zabezpieczeń w przeglądarce.
Przykładowo w Google Chrome możemy wykorzystać flagi: `--ignore-certificate-errors`, `--unsafely-treat-insecure-origin-as-secure`, `--allow-insecure-localhost`. Część z tych flag może być redundantna lub w ogóle nie działać (ze względu na różne wersje Google Chrome).
Przykładowe uruchomienie przeglądarki z adresem `https://localhost:8080`, w wierszu poleceń może wyglądać tak:

```sh
user@user:$ google-chrome --ignore-certificate-errors --unsafely-treat-insecure-origin-as-secure=https://localhost:8080 --allow-insecure-localhost https://localhost:8080
```

Analogiczne flagi uruchomienia można znaleźć do innych przeglądarek.
Jednak do tego ćwiczenia, ze względu na PWA, które zostało "zapoczątkowane" przez Google, polecam skorzystać z Google Chrome.